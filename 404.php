<?php get_header(); ?>

<div class="opener">
	<div class="shell">

		<h1>
			<?php echo __( 'Oups, something went wrong.', 'hm-theme' ); ?>
		</h1>

	</div>
</div>

<?php get_footer(); ?>