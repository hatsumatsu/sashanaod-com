<?php get_header(); ?>

<div class="opener">
	<div class="shell">
		<h1>
			<?php echo __( 'News', 'hm-theme' ); ?>
		</h1>

		<?php get_inc( 'filter', 'post', false ); ?>

	</div>
</div>

<section class="posts posts--post"
		data-url="<?php echo esc_attr( get_current_archive_url() ); ?>"
		data-page="<?php echo esc_attr( get_query_var( 'paged' ) ); ?>" 
		data-pages-total="<?php echo esc_attr( $wp_query->max_num_pages ); ?>">
	<div class="shell">
		<div class="grid" data-grid-role="container">

<?php 
	if( have_posts() ) {
		while( have_posts() ) { 
			the_post();
			
			get_inc( 'post', get_post_type(), true );
		} 

	get_inc( 'pagination', 0, 0 );
 
	} else { 
		get_inc( 'post', 'noposts', true );
	}
?>

		</div>
	</div>
</section>

<?php get_footer(); ?>