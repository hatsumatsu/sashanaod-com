<div class="filters">

<?php

	$_terms = get_terms(
		array( 'category' ), 
		array(
    		'orderby'           => 'name', 
    		'order'             => 'ASC',
    		'hide_empty'        => true
    	)
    );

	$terms = array();
	foreach( $_terms as $term ) {
		if( $term->name !== 'Uncategorized' ) {
			$terms[] = $term;
		}
	}

    if( $terms ) {
    	$current = get_term_by( 'slug', get_query_var( 'category_name' ), 'category' );
    	$toggle_label = ( $current ) ? $current->name : __( 'All categories', 'hm-theme' );

		$class = '';
		$class .=  ( get_query_var( 'category_name' ) ) ? ' filtered' : '';  	
?>

	<div class="filter filter--post<?php echo esc_attr( $class ); ?>">
		<a href="#filter" class="filter-toggle">
			<?php echo $toggle_label; ?>
		</a>

		<nav class="filter-nav">
			<h4>
				<?php echo __( 'Filter by category', 'hm-theme' ); ?>
			</h4>

<?php
	if( $current ) {
?>
			<a href="<?php echo esc_url( get_permalink( get_option( 'page_for_posts' )
) ); ?>">
				<?php echo __( 'All categories', 'hm-theme' ); ?>
			</a>
<?php
	}
?>

<?php
		foreach( $terms as $term ) {
			$class = '';
			$class = ( get_query_var( 'category_name' ) == $term->slug ) ? ' current' : '';
?>
			<a href="<?php echo esc_url( get_term_link( $term ) ); ?>" class="<?php echo esc_attr( $class ); ?>">
				<?php echo wptexturize( $term->name ); ?>
			</a>
<?php
		}
?>
			<div class="filter-ui">
				<a href="#?" class="close">
					<?php echo __( 'Close filter', 'hm-theme' ); ?>
				</a>
			</div>
		</nav>
	</div>

<?php
	}
?>

</div>