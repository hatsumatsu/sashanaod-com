<?php
    
    // terms
    $_terms =  get_the_terms( get_the_ID(), 'category' );
    $terms = array();
    if( $_terms ) {
        foreach( $_terms as $term ) {
            if( $term->name != 'Uncategorized' ) {
                $terms[] = $term;
            }
        }
    }

    // post image
    $image_id = get_post_thumbnail_id( get_the_ID() );

    $class = '';
    $class .= ( $terms ) ? ' has-terms' : ' has-no-terms';
    $class .= ( $image_id ) ? ' has-image' : ' has-no-image';
?>

<article class="post post--<?php echo get_post_type_advanced(); ?><?php echo esc_attr( $class ); ?>" data-grid-role="item">

<?php
    if( $image_id ) {
?>
    <a href="<?php the_permalink(); ?>" class="post-image post-image--<?php echo get_post_type_advanced(); ?>" title="<?php echo esc_attr( sprintf( __( 'Read %s', 'hm-theme' ), get_the_title() ) ); ?>">
<?php
        the_responsive_image(
            $image_id,
            array(
                'tiny',
                'thumbnail',
                'medium',
                'large',
                'larger',
                'full'
            ),
            array(
                'class' => '',
                'alt'   => sprintf( __( 'Cover photo from “%s”', 'hm-theme' ), get_the_title() )
            ),
            true,
            true
        );
?>
    </a>
<?php   
    }
?>

<?php
    if( $terms ) {
?>
    <div class="post-terms post-terms--<?php echo get_post_type_advanced(); ?>">
<?php
        foreach( $terms as $term ) {
?>
        <a href="<?php echo esc_url( get_term_link( $term ) ); ?>">
            <?php echo wptexturize( $term->name ); ?>
        </a>
<?php
        }
?>        
    </div>
<?php
    }
?>
    <time class="post-time post-time--<?php echo get_post_type_advanced(); ?>" datetime="<?php the_time( 'Y-m-d\TH:i' ); ?>">
        <?php the_time( 'j' ); ?>&thinsp;/&thinsp;<?php the_time( 'n' ); ?>&thinsp;/&thinsp;<?php the_time( 'Y' ); ?>
    </time>

    <h2>
    	<a href="<?php the_permalink(); ?>">
    		<?php the_title(); ?>
    	</a>
    </h2>

</article>