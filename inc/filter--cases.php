<div class="filters">

<?php
	// clients
	
	$terms = get_terms(
		array( 'clients' ), 
		array(
    		'orderby'           => 'name', 
    		'order'             => 'ASC',
    		'hide_empty'        => true
    	)
    );

    if( $terms ) {
    	$current = get_term_by( 'slug', get_query_var( 'clients' ), 'clients' );
    	$toggle_label = ( $current ) ? $current->name : __( 'All Clients', 'hm-theme' );

    	$class = '';
    	$class .= ( get_query_var( 'clients' ) ) ? ' filtered' : '';
?>

	<div class="filter filter--post<?php echo esc_attr( $class ); ?>">
		<a href="#filter" class="filter-toggle">
			<?php echo $toggle_label; ?>
		</a>

		<nav class="filter-nav">
			<h4>
				<?php echo __( 'Filter by clients', 'hm-theme' ); ?>
			</h4>

<?php
	if( $current ) {
?>
			<a href="<?php echo esc_url( get_site_url( 0, __( 'cases', 'hm-theme' ) ) ); ?>">
				<?php echo __( 'All Clients', 'hm-theme' ); ?>
			</a>
<?php
	}
?>

<?php
		foreach( $terms as $term ) {
			$class = '';
			$class = ( get_query_var( 'clients' ) == $term->slug ) ? ' current' : '';
?>
			<a href="<?php echo esc_url( get_term_link( $term ) ); ?>" class="<?php echo esc_attr( $class ); ?>">
				<?php echo wptexturize( $term->name ); ?>
			</a>
<?php
		}
?>
			<div class="filter-ui">
				<a href="#?" class="close">
					<?php echo __( 'Close filter', 'hm-theme' ); ?>
				</a>
			</div>
		</nav>
	</div>

<?php
	}
?>


<?php
	// sectors
	// 
	$terms = get_terms(
		array( 'sectors' ), 
		array(
    		'orderby'           => 'name', 
    		'order'             => 'ASC',
    		'hide_empty'        => true
    	)
    );

    if( $terms ) {
    	$current = get_term_by( 'slug', get_query_var( 'sectors' ), 'sectors' );
    	$toggle_label = ( $current ) ? $current->name : __( 'All Sectors', 'hm-theme' );

    	$class = '';
    	$class .= ( get_query_var( 'sectors' ) ) ? ' filtered' : '';    	
?>

	<div class="filter filter--post<?php echo esc_attr( $class ); ?>">
		<a href="#filter" class="filter-toggle">
			<?php echo $toggle_label; ?>
		</a>

		<nav class="filter-nav">
			<h4>
				<?php echo __( 'Filter by sector', 'hm-theme' ); ?>
			</h4>

<?php
	if( $current ) {
?>
			<a href="<?php echo esc_url( get_site_url( 0, __( 'cases', 'hm-theme' ) ) ); ?>">
				<?php echo __( 'All Sectors', 'hm-theme' ); ?>
			</a>
<?php
	}
?>

<?php
		foreach( $terms as $term ) {
			$class = '';
			$class = ( get_query_var( 'sectors' ) == $term->slug ) ? ' current' : '';
?>
			<a href="<?php echo esc_url( get_term_link( $term ) ); ?>" class="<?php echo esc_attr( $class ); ?>">
				<?php echo wptexturize( $term->name ); ?>
			</a>
<?php
		}
?>
			<div class="filter-ui">
				<a href="#?" class="close">
					<?php echo __( 'Close filter', 'hm-theme' ); ?>
				</a>
			</div>
		</nav>
	</div>

<?php
	}
?>

<?php
	// disciplines
	// 
	$terms = get_terms(
		array( 'disciplines' ), 
		array(
    		'orderby'           => 'name', 
    		'order'             => 'ASC',
    		'hide_empty'        => true
    	)
    );

    if( $terms ) {
    	$current = get_term_by( 'slug', get_query_var( 'disciplines' ), 'disciplines' );
    	$toggle_label = ( $current ) ? $current->name : __( 'All Disciplines', 'hm-theme' );

    	$class = '';
    	$class .= ( get_query_var( 'disciplines' ) ) ? ' filtered' : '';     	
?>

	<div class="filter filter--post<?php echo esc_attr( $class ); ?>">
		<a href="#filter" class="filter-toggle">
			<?php echo $toggle_label; ?>
		</a>

		<nav class="filter-nav">
			<h4>
				<?php echo __( 'Filter by discipline', 'hm-theme' ); ?>
			</h4>

<?php
	if( $current ) {
?>
			<a href="<?php echo esc_url( get_site_url( 0, __( 'cases', 'hm-theme' ) ) ); ?>">
				<?php echo __( 'All Disciplines', 'hm-theme' ); ?>
			</a>
<?php
	}
?>

<?php
		foreach( $terms as $term ) {
			$class = '';
			$class = ( get_query_var( 'disciplines' ) == $term->slug ) ? ' current' : '';
?>
			<a href="<?php echo esc_url( get_term_link( $term ) ); ?>" class="<?php echo esc_attr( $class ); ?>">
				<?php echo wptexturize( $term->name ); ?>
			</a>
<?php
		}
?>
			<div class="filter-ui">
				<a href="#?" class="close">
					<?php echo __( 'Close filter', 'hm-theme' ); ?>
				</a>
			</div>
		</nav>
	</div>

<?php
	}
?>

</div>