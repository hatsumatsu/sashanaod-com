<?php
	global $related_post;

    // post image
    $image_id = get_post_thumbnail_id( $related_post->ID );

    // meta 
    $clients =  get_the_terms( $related_post->ID, 'clients' );

    $class = '';
    $class .= ( $image_id ) ? ' has-image' : ' has-no-image';
?>

<article class="post post--related post--related-cases<?php echo esc_attr( $class ); ?>">

<?php
    if( $image_id ) {
?>
    <a href="<?php echo esc_url( get_permalink( $related_post->ID ) ); ?>" class="post-image post-image--related post-image--related-cases" title="<?php echo esc_attr( sprintf( __( 'Read %s', 'hm-theme' ), get_the_title( $related_post->ID ) ) ); ?>">
<?php
        the_responsive_image(
            $image_id,
            array(
                'tiny',
                'thumbnail',
                'medium',
                'large',
                'larger',
                'full'
            ),
            array(
                'class' => '',
                'alt'   => sprintf( __( 'Cover photo from “%s”', 'hm-theme' ), get_the_title( $related_post->ID ) )
            ),
            true,
            true
        );
?>
    </a>
<?php   
    }
?>

    <h2>
    	<a href="<?php echo esc_url( get_permalink( $related_post->ID ) ); ?>">
            <span class="title">
                <?php echo get_the_title( $related_post->ID ); ?>
            </span>
<?php
    if( $clients && !get_post_meta( $related_post->ID, 'cases--hide-client', true ) ) {
?>
            <span class="client">
                <?php echo wptexturize( $clients[0]->name ); ?>
            </span>
<?php
    }
?>
    	</a>
    </h2>

</article>