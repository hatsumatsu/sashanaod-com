<?php
    
    // meta
    $clients =  get_the_terms( get_the_ID(), 'clients' );
    $sectors =  get_the_terms( get_the_ID(), 'sectors' );
    $disciplines =  get_the_terms( get_the_ID(), 'disciplines' );

    // post image
    $image_id = get_post_thumbnail_id( get_the_ID() );

    $class = '';
    $class .= ( $image_id ) ? ' has-image' : ' has-no-image';
?>

<article class="post post--<?php echo get_post_type_advanced(); ?><?php echo esc_attr( $class ); ?>" data-grid-role="item">

<?php
    if( $image_id ) {
?>
    <a href="<?php the_permalink(); ?>" class="post-image post-image--<?php echo get_post_type_advanced(); ?>" title="<?php echo esc_attr( sprintf( __( 'Read %s', 'hm-theme' ), get_the_title() ) ); ?>">
<?php
        the_responsive_image(
            $image_id,
            array(
                'tiny',
                'thumbnail',
                'medium',
                'large',
                'larger',
                'full'
            ),
            array(
                'class' => '',
                'alt'   => sprintf( __( 'Cover photo from “%s”', 'hm-theme' ), get_the_title() )
            ),
            true,
            true
        );
?>
    </a>
<?php   
    }
?>

<?php
    if( $sectors ) {
?>
    <div class="post-sector post-sector--<?php echo get_post_type_advanced(); ?>">
        <a href="<?php echo esc_url( get_term_link( $sectors[0] ) ); ?>">
            <?php echo wptexturize( $sectors[0]->name ); ?>
        </a>  
    </div>
<?php
    }
?>

<?php
    if( $disciplines ) {
?>
    <div class="post-discipline post-discipline--<?php echo get_post_type_advanced(); ?>">
        <a href="<?php echo esc_url( get_term_link( $disciplines[0] ) ); ?>">
            <?php echo wptexturize( $disciplines[0]->name ); ?>
        </a>  
    </div>
<?php
    }
?>


    <h2>
    	<a href="<?php the_permalink(); ?>">
            <span class="title">
                <?php the_title(); ?>
            </span>
<?php
    if( $clients && !get_post_meta( get_the_ID(), 'cases--hide-client', true ) ) {
?>
            <span class="client">
                <?php echo wptexturize( $clients[0]->name ); ?>
            </span>
<?php
    }
?>
    	</a>
    </h2>

</article>