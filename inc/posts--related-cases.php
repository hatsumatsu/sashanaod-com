<?php

	$related_posts = get_posts( 
		array(
			'post_type' => 'cases',
			'posts_per_page' => 3,
			'exclude' => array( get_the_ID() )
		)
	);		

	if( $related_posts ) {
?>
<section class="posts posts--related posts--related-cases">
	<div class="shell">

		<h3>
			<?php echo __( 'Related Cases', 'hm-theme' ); ?>
		</h3>

		<div class="grid">
<?php
		global $related_post;
		foreach( $related_posts as $related_post ) {
			get_inc( 'post', 'related-' . get_post_type_advanced(), false );
		}
?>
		</div>
	</div>
</section>
<?php
	}
?>