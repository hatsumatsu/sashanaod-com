<?php 
    global $entry;
?>

<h4>
	<?php echo __( 'Latest from Twitter', 'hm-theme' ); ?>
</h4>

<article class="widget-tweet tweet">
    <div class="tweet-content">
        <?php echo strip_tags( $entry->post_content, '<a>' ); ?>
    </div>
    <div class="tweet-meta">
        <a href="https://twitter.com/sashanaod" class="username" target="_blank">
            @sashanaod
        </a>

        <time class="time" datetime="<?php echo get_the_time( 'Y-m-d\TH:i', $entry->ID ); ?>">
            <?php // echo get_the_time( 'j / n / Y', $entry->ID ); ?>
            <?php echo get_the_time( 'j', $entry->ID  ) . '&thinsp;/&thinsp;' . get_the_time( 'n', $entry->ID  ) . '&thinsp;/&thinsp;' . get_the_time( 'Y', $entry->ID  ); ?>            
        </time>        
    </div>
</article>