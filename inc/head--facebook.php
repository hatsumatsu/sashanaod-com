<meta property="og:site_name" content="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
<meta property="og:description" content="<?php echo esc_attr( get_bloginfo( 'description' ) ); ?>">
<?php 
    if( is_single() || is_page() ) {
?>
<meta property="og:title" content="<?php echo esc_attr( $post->post_title ); ?>">
<?php  
    }
?>
<meta property="og:type" content="website">
<meta property="og:image" content="<?php echo esc_url( get_site_image() ); ?>">