<?php 
    $home_id = get_option( 'page_on_front' );

    if( $home_id = get_option( 'page_on_front' ) ) { 
        if( $id = get_post_meta( $home_id, 'home--cases', true ) ) {
            if( $entry = get_post( $id ) ) {
                $clients =  get_the_terms( $entry->ID, 'clients' );    
?>

<a href="<?php echo esc_url( get_permalink( $entry->ID ) ); ?>" class="widget-image widget-image--cases">

<?php
            	if( $image_id = get_post_thumbnail_id( $entry->ID ) ){
                    the_responsive_image(
                        $image_id,
                        array(
                            'thumbnail',
                            'medium',
                            'large',
                            'larger',
                            'full'
                        ),
                        array( 
                            'sizes' => '100vw',
                            'class' => 'widget-image-image widget-image-image--cases',
                            'alt'   => sprintf( __( 'Post image for “%s”', 'hm-theme' ), get_the_title( $entry->ID ) )
                        ),
                        true,
                        true
                    );
            	}
?>

</a>

<h4 class="widget-title widget-title--blog">
    <a href="<?php echo esc_url( get_permalink( $entry->ID ) ); ?>">    
        <?php echo __( 'Spotlight', 'hm-theme' ); ?>
    </a>
</h4>

<h2>
    <a href="<?php echo esc_url( get_permalink( $entry->ID ) ); ?>">
        <span class="title">
            <?php echo get_the_title( $entry->ID ); ?>
        </span>
<?php
                if( $clients ) {
?>
        <span class="client">
            <?php echo wptexturize( $clients[0]->name ); ?>
        </span>
<?php
                }
?>    
    </a>
</h2>

<?php
            }
        }
    }