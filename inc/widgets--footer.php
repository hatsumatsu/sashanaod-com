<section class="widgets">

<!--	
	<div class="shell">
		<h2 class="widgets-tagline">
			<?php echo get_bloginfo( 'description' ); ?>
		</h2>
	</div>
-->

<?php 
	if( is_active_sidebar( 'widgets' ) ) { 
?>
<div class="widgets-canvas">
	
	<?php dynamic_sidebar( 'widgets' ); ?>

	<aside class="widget widget--status">
		<span class="status--location">Amsterdam</span>
		<span class="status--time"></span>
		<span class="status--weather"></span>
	</aside>

</div>
<?php
	}
?>

<?php
    if( is_front_page() ) {
?>
        <a href="#content" class="learn-more">
            <?php echo __( 'Learn more', 'hm-theme' ); ?>
        </a>
<?php        
    }
?>

</section>