<?php 
    $home_id = get_option( 'page_on_front' );

    if( $home_id = get_option( 'page_on_front' ) ) { 
        if( $id = get_post_meta( $home_id, 'home--posts', true ) ) {
            if( $entry = get_post( $id ) ) {
?>

<a href="<?php echo esc_url( get_permalink( $entry->ID ) ); ?>" class="widget-image widget-image--blog">

<?php
            	if( $image_id = get_post_thumbnail_id( $entry->ID ) ) {
                    the_responsive_image(
                        $image_id,
                        array(
                            'thumbnail',
                            'medium',
                            'large',
                            'larger',
                            'full'
                        ),
                        array( 
                            'sizes' => '100vw',
                            'class' => 'widget-image-image widget-image-image--blog',
                            'alt'   => sprintf( __( 'Post image for “%s”', 'hm-theme' ), get_the_title( $entry->ID ) )
                        ),
                        true,
                        true
                    );
            	}
?>

</a>

<h4 class="widget-title widget-title--blog">
    <a href="<?php echo esc_url( get_permalink( $entry->ID ) ); ?>">    
        <?php echo __( 'Top Story', 'hm-theme' ); ?>    
    </a>
</h4>

<h2>
    <a href="<?php echo esc_url( get_permalink( $entry->ID ) ); ?>">
        <?php echo get_the_title( $entry->ID ); ?>
    </a>
</h2>

<?php
            }
        }
    }
?>