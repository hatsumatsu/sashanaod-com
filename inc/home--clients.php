<?php
	$clients = get_terms(
		array(
			'clients'
		),
		array(
			//'hide_empty' => false,			
			'meta_query' => array(
				array(

				)
			)
		)
	);


	if( $clients ) {
?>
<section class="clients clients--home">
	<h2>
		<?php echo __( 'Some of the great brands I&apos;ve&nbsp;worked&nbsp;with:', 'hm-theme'  ); ?>
	</h2>

<?php
		foreach( $clients as $client ) {
			$image_id = get_term_meta( $client->term_id, '_thumbnail_id', true );
			if( $image_id ) { 
				if( $url = wp_get_attachment_url( $image_id ) ) {
?>
	<a href="<?php echo esc_url( get_term_link( $client ) ); ?>" class="client client--home client--<?php echo esc_attr( $client->slug ); ?>" title="<?php echo esc_attr( sprintf( __( 'View all %s cases', 'hm-theme' ), $client->name ) ); ?>">
		<span class="title">
			<?php echo wptexturize( $client->name ); ?>
		</span>
		<img src="<?php echo esc_url( $url ); ?>" alt="<?php echo esc_attr( sprintf( __( 'Logo of %s', 'hm-theme' ), $client->name ) ); ?>">
	</a>
<?php
				}
			}
		}
?>
</section>
<?php
	}
