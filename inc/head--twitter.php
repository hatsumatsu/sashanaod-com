<meta name="twitter:card" content="summary">
<meta name="twitter:url" content="<?php echo esc_url( get_permalink( $post->ID ) ); ?>">
<meta name="twitter:title" content="<?php echo esc_attr( $post->post_title ); ?>">
<meta name="twitter:description" content="<?php echo esc_attr( sprintf( '%s – %s', get_bloginfo( 'name' ), get_bloginfo( 'description' ) ) ); ?>">
<meta name="twitter:image" content="<?php echo esc_url( get_site_image() ); ?>">

<meta name="twitter:site" content="@sashanaod">
<meta name="twitter:creator" content="@sashanaod">