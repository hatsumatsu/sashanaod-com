<?php
	global $terms;

	$related_posts = get_posts( 
		array(
			'post_type' => 'post',
			'posts_per_page' => 3,
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field'    => 'term_id',
					'terms'    => array( $terms[0]->term_id )
				)
			),
			'exclude' => array( get_the_ID() )
		)
	);

	if( count( $related_posts ) < 3 ) {
		$related_posts = get_posts( 
			array(
				'post_type' => 'post',
				'posts_per_page' => 3,
				'exclude' => array( get_the_ID() )
			)
		);		
	}

	if( $related_posts ) {
?>
<section class="posts posts--related posts--related-post">
	<div class="shell">

		<h3>
			<?php echo __( 'Related Posts', 'hm-theme' ); ?>
		</h3>

		<div class="grid">
<?php
		global $related_post;
		foreach( $related_posts as $related_post ) {
			get_inc( 'post', 'related-' . get_post_type_advanced(), false );
		}
?>
		</div>
	</div>
</section>
<?php
	}
?>