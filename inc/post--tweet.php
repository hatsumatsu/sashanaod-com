<?php
    // post image
    $image_id = get_post_thumbnail_id( get_the_ID() );

    $class = '';
    $class .= ( $terms ) ? ' has-terms' : ' has-no-terms';
    $class .= ( $image_id ) ? ' has-image' : ' has-no-image';
?>

<article class="post post--<?php echo get_post_type_advanced(); ?><?php echo esc_attr( $class ); ?>" data-grid-role="item">

<?php
    if( $image_id ) {
?>
    <a class="post-image post-image--<?php echo get_post_type_advanced(); ?>" title="<?php echo esc_attr( sprintf( __( 'Read %s', 'hm-theme' ), get_the_title() ) ); ?>">
<?php
        the_responsive_image(
            $image_id,
            array(
                'tiny',
                'thumbnail',
                'medium',
                'large',
                'larger',
                'full'
            ),
            array(
                'class' => '',
                'alt'   => sprintf( __( 'Cover photo from “%s”', 'hm-theme' ), get_the_title() )
            ),
            true,
            true
        );
?>
    </a>
<?php   
    }
?>    

    <div class="post-source post-source--<?php echo get_post_type_advanced(); ?>">
        <?php echo __( 'Twitter', 'hm-theme' ); ?>
        <span class="post-source-username post-source-username--<?php echo get_post_type_advanced(); ?>">
            <a href="https://twitter.com/sashanaod" target="_blank">
                @sashanaod
            </a>
        </span>
    </div>

    <time class="post-time post-time--<?php echo get_post_type_advanced(); ?>" datetime="<?php the_time( 'Y-m-d\TH:i' ); ?>">
        <?php the_time( 'j' ); ?>&thinsp;/&thinsp;<?php the_time( 'n' ); ?>&thinsp;/&thinsp;<?php the_time( 'Y' ); ?>
    </time>

    <h2>
    	<?php echo strip_tags( get_the_content(), '<a>' ); ?>
    </h2>
</article>