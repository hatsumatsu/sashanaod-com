<?php get_header(); ?>

<?php 
	if( have_posts() ) {
		while( have_posts() ) { 
			the_post(); 

		    $image_id = get_post_thumbnail_id( get_the_ID() );
?>
<div class="shell">

<?php
    if( $image_id ) {
?>
    <a class="post-image post-image--<?php echo get_post_type_advanced(); ?>">
<?php
        the_responsive_image(
            $image_id,
            array(
                'tiny',
                'thumbnail',
                'medium',
                'large',
                'larger',
                'full'
            ),
            array(
                'class' => '',
                'alt'   => sprintf( __( 'Cover photo from “%s”', 'hm-theme' ), get_the_title() )
            ),
            true,
            true
        );
?>
    </a>
<?php   
    }
?>

	<h1><?php the_title(); ?></h1>
		
	<div class="single-content single-content--<?php echo esc_attr( get_post_type_advanced() ); ?> inline-content">   
		<?php the_content(); ?>
	</div>
</div>

<div class="divider--footer">

</div>
<?php 
		}

	}
?>

<?php get_footer(); ?>