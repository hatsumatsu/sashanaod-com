<?php get_header(); ?>

<div class="shell">
<?php 
	if( have_posts() ) {
		while( have_posts() ) { 
			the_post(); 
?>
	<div class="home-content">   
		<?php the_content(); ?>
	</div>

</div>

<div class="section--home-clients">
	<div class="shell">
		<div class="home-clients">   
			<?php get_inc( 'home', 'clients', 0 ); ?>
		</div>
	</div>
</div>

<?php 
		}
	}
?>
</div>

<?php get_footer(); ?>