		</section><!-- end .content -->
<?php
	if( $page = get_page_by_title( 'Contact' ) ) {
?>

		<section class="section--contact" id="contact">
			<div class="shell">
				<?php echo apply_filters( 'the_content', $page->post_content ); ?>
			</div>
		</section>


<?php 
	}
?>
	
		<div class="divider--footer">

		</div>


		<div class="shell">
			<?php get_inc( 'nav', 'footer-primary', 0 ); ?>
		</div>

		<small class="footer-credits">
			© Sasha Naod <?php echo date( 'Y' ); ?>
		</small>

	</section><!-- end .main -->

	<section class="footer">

	</section><!-- end .footer -->	

	<?php wp_footer(); ?>
</body>
</html>