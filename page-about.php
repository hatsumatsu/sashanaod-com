<?php get_header(); ?>

<?php 
	if( have_posts() ) {
		while( have_posts() ) { 
			the_post(); 

		    $image_id = get_post_thumbnail_id( get_the_ID() );
?>


<div class="about--page">
	<div class="shell">
		<div class="page-content page-content--about inline-content">
			<?php the_content(); ?>
		</div>

	<?php
		if( $sidebar_content = get_post_meta( get_the_ID(), 'about--sidebar-content', true ) ) {
	?>
		<div class="page-sidebar page-sidebar--about inline-content">
			<?php echo apply_filters( 'the_content', $sidebar_content ); ?>		
		</div>	
	<?php
		}
	?>
	</div>
</div>


<?php
    if( $image_id ) {
?>
<div class="about--image">
	<div class="shell">
    	<a class="post-image post-image--single post-image--<?php echo get_post_type_advanced(); ?>" data-center-vertically="true">
<?php
        the_responsive_image(
            $image_id,
            array(
                'tiny',
                'thumbnail',
                'medium',
                'large',
                'larger',
                'full'
            ),
            array(
                'class' => '',
                'alt'   => sprintf( __( 'Cover photo from “%s”', 'hm-theme' ), get_the_title() )
            ),
            true,
            true
        );
?>
    	</a>
    </div>
</div>
<?php   
    }
?>


<section class="about--service" id="service">
	<div class="shell">
		<h2 class="heading--approach">
			<?php echo __( 'Approach', 'hm-theme' ); ?>
		</h2>

<?php
	// SERVICE BLOCKS
	if( $total = get_post_meta( get_the_ID(), 'about--service-blocks', true ) ) {
?>
		<div class="service-blocks">
			<div class="grid">
<?php
		for( $i = 0; $i < $total; $i++ ) {
			$title = get_post_meta( get_the_ID(), 'about--service-blocks_' . $i . '_block-title', true );
			$text = get_post_meta( get_the_ID(), 'about--service-blocks_' . $i . '_block-text', true );
			$icon = get_post_meta( get_the_ID(), 'about--service-blocks_' . $i . '_block-icon', true );
?>

				<div class="service-block service-block--<?php echo esc_attr( $icon ); ?>">
					<h3 class="service-block-title">
						<?php echo wptexturize( $title ); ?>
					</h3>

					<div class="service-block-text inline-content">
						<?php echo apply_filters( 'the_content', $text ); ?>
					</div>
				</div>

<?php
		}
?>


			</div>
		</div>
<?php
	}
?>

<?php
	// SERVICE LIST
	if( $total = get_post_meta( get_the_ID(), 'about--service-list', true ) ) {
?>
	<ul class="service-list grid">
		<h2 class="heading--services">
			<?php echo __( 'Expertise', 'hm-theme' ); ?>
		</h2>
<?php
		for( $i = 0; $i < $total; $i++ ) {
			$title = get_post_meta( get_the_ID(), 'about--service-list_' . $i . '_service-list-title', true );
?>
		<li class="service-list-item">
			<?php echo wptexturize( $title ); ?>
		</li>
<?php
		}
?>
	</ul>
<?php
	}
?>

	</div>
</section>

<?php
	// CLIENTS
	$clients = get_terms(
		array(
			'clients'
		),
		array(
			'hide_empty' => false
		)
	);

	if( $clients ) {
?>

<section class="about--clients" id="clients">
	<div class="shell">

		<ul class="clients clients--about grid">
			<h2 class="heading--clients">
				<?php echo __( 'Brands I&apos;ve worked&nbsp;with', 'hm-theme' ); ?>
			</h2>			
<?php
		foreach( $clients as $client ) {
?>
			<li class="client client--about">
<?php
			if( $client->count > 0 ) {
?>
				<a href="<?php echo esc_url( get_term_link( $client ) ); ?>" title="<?php echo esc_attr( sprintf( __( 'View all %s cases', 'hm-theme' ), $client->name ) ); ?>"> 
<?php
			}
?>
					<?php echo wptexturize( $client->name ); ?>

<?php
			if( $client->count > 0 ) {
?>
				</a> 
<?php
			}
?>
			</li>
<?php
		}
?>
		</ul>

	</div>
</section>

<?php
	}
?>

<?php
	// TESTIMONIALS
	$testimonials = get_posts(
		array(
			'post_type' 		=> 'testimonials',
			'posts_per_page' 	=> 5,
			'ordeby' 			=> 'menu_order',
			'order' 			=> 'ASC'
		)
	);

	if( $testimonials ) {
?>
<section class="about--testimonials" id="testimonials" data-carousel="wrapper">
	
	<div class="shell">

<?php
		foreach( $testimonials as $testimonial ) {
?>
		<article class="testimonial testimonial--about" data-carousel="item">
			<div class="testimonial-text testimonial-text--about inline-content">
				<?php echo apply_filters( 'the_content', $testimonial->post_content ); ?>
			</div>
			<div class="testimonial-author testimonial-author--about">
				<?php echo get_the_title( $testimonial->ID ); ?>				
			</div>
		</article>	
<?php
		}
?>
	</div>

</section>

<?php
	}
?>

<?php 
		}

	}
?>

<?php get_footer(); ?>