jQuery( function( $ ) {

    // app
    siteLarger = ( function() {

        var init = function() {
            debuglog( 'siteLarger.init()' );
            bindEventHandlers();
            win.init();
            widgets.init();
            head.init();
            angles.init();
            posts.init();
            status.init();
            carousel.init();
            postImages.init();
        }

        var bindEventHandlers = function() {

        }

        // module window
        var win = ( function() {

            var settings = {
                width: 0,
                height: 0,
                documentHeight: 0,

                // scroll
                scrollTop: -1,
                _scrollTop: -1,
                scrollDetectionMode: 'scrollEvent', // 'scrollEvent' or 'requestAnimationFrame'
                nowScroll: Date.now(),
                nowLoop: Date.now(),
                fps: ( 1000 / 60 ),
                scrollDirection: false,
                ignoreScrollDirection: false,
                scrollDownDelay: 0,
                scrollUpDelay: 0,
                scrollFactor: -1,
                isScrolledToTop: false,
                isScrolledToBottom: false,
                scrollTopOffset: 20,
                scrollBottomOffset: 20,
                isScrolledToFirstScreen: false,
                isScrolledToLastScreen: false,
                scrollToOffset: 0,
                scrollSpeed: 2,

                // mouse
                mouseX: 0,
                mouseY: 0,
                _mouseX: -1,
                _mouseY: -1,
                mousePosition: {
                    x: 0,
                    y: 0
                }                                   
            };

            var init = function() {
                debuglog( 'siteLarger.win.init()' );
                
                settings.element = $( window );
                
                onResizeFinish();
                
                bindEventHandlers();

                onScroll();
                loop();
            }

            var bindEventHandlers = function() {

                if( settings.scrollDetectionMode == 'scrollEvent' ) {

                    settings.element.on( 'scroll', function() {                        
                        var now = Date.now();
                        var elapsed = now - settings.nowScroll;
                        
                        if( elapsed > settings.fps ) {
                            settings.nowScroll = now - ( elapsed % settings.fps );

                            settings._scrollTop = settings.scrollTop;
                            settings.scrollTop = settings.element.scrollTop();
                            $( document ).trigger( 'win/scroll' );
                        }
                    } );

                }

                $( window )
                    .on( 'mousemove', function( e ) { 
                        var x = e.pageX;
                        var y = e.pageY;

                        settings.mouseX = x;
                        settings.mouseY = y;

                        var factorX = ( x / settings.width );
                        var factorY = ( y / settings.height );

                        settings.mousePosition.x = factorX - 0.5;
                        settings.mousePosition.y = factorY - 0.5;
                    } );                   

                $( document )
                    .on( 'win/scroll', function() {
                        onScroll();
                    } )
                    .on( 'win/resize/finish', function() {
                        onResizeFinish();
                    } )

                    // top
                    .on( 'win/scrolledToTop', function() {
                        debuglog( 'scrolledToTop' );

                        $( 'html' ).addClass( 'scrolled-to-top' );
                    } )
                    .on( 'win/scrolledFromTop', function() {
                        debuglog( 'scrolledFromTop' );
                        
                        $( 'html' ).removeClass( 'scrolled-to-top' );
                    } )

                    // bottom
                    .on( 'win/scrolledToBottom', function() {
                        debuglog( 'scrolledToBottom' );
                        
                        $( 'html' ).addClass( 'scrolled-to-bottom' );                        
                    } )
                    .on( 'win/scrolledFromBottom', function() {
                        debuglog( 'scrolledFromBottom' );

                        $( 'html' ).removeClass( 'scrolled-to-bottom' );                                                
                    } )

                    // first screen
                    .on( 'win/scrolledToFirstScreen', function() {
                        debuglog( 'scrolledToFirstScreen' );

                        $( 'html' ).addClass( 'scrolled-to-firstscreen' );                                                
                    } )
                    .on( 'win/scrolledFromFirstScreen', function() {
                        debuglog( 'scrolledFromFirstScreen' );

                        $( 'html' ).removeClass( 'scrolled-to-firstscreen' );                                                
                    } )

                    // last screen
                    .on( 'win/scrolledToLastScreen', function() {
                        debuglog( 'scrolledToLastScreen' );

                        $( 'html' ).addClass( 'scrolled-to-lastscreen' );                                                
                    } )
                    .on( 'win/scrolledFromLastScreen', function() {
                        debuglog( 'scrolledFromLastScreen' );

                        $( 'html' ).removeClass( 'scrolled-to-lastscreen' );                                                
                    } )
                    .on( 'win/changeScrollDirection', function() {
                        onScrollDirectionChange();
                    } );
            }

            /**
             * requestAnimationFrame loop throttled at 60fps 
             */
            var loop = function() {
                requestAnimationFrame( loop );
                
                var now = Date.now();
                var elapsed = now - settings.nowLoop;
                
                // the actual 'loop'
                if( elapsed > settings.fps ) {
                    settings.nowLoop = now - ( elapsed % settings.fps );
                  
                    // scrollTop
                    if( settings.scrollDetectionMode == 'requestAnimationFrame' ) {                  
                        settings._scrollTop = settings.scrollTop;
                        settings.scrollTop = settings.element.scrollTop();

                        if( settings.scrollTop != settings._scrollTop ) {
                            $( document ).trigger( 'win/scroll' );
                        }
                    }

                    // mouse position
                    if( settings.mouseX != settings._mouseX
                    || settings.mouseY != settings._mouseY ) {        
                        settings._mouseX = settings.mouseX;
                        settings._mouseY = settings.mouseY;
                        $( document ).trigger( 'win/mousemove' );
                    }                    

                    // regular loop
                    $( document ).trigger( 'win/loop' );
                }
            }



            var onResizeFinish = function() {
                debuglog( 'siteLarger.win.onResizeFinish()' );

                settings.width = settings.element.width();
                settings.height = settings.element.height();
                settings.documentHeight = $( 'html' ).outerHeight();  

                // onScroll();              
            }

            var onScroll = function() {
                debuglog( 'onScroll()' );

                settings.scrollFactor = settings.scrollTop / ( settings.height - settings.documentHeight ) * -1;

                // top
                if( settings.scrollTop > settings.scrollTopOffset ) {
                    if( settings.isScrolledToTop ) {
                        settings.isScrolledToTop = false;
                        $( document ).trigger( 'win/scrolledFromTop' );
                    }                  
                }   

                if( settings.scrollTop < settings.scrollTopOffset ) {
                    if( !settings.isScrolledToTop ) {
                        settings.isScrolledToTop = true;
                        $( document ).trigger( 'win/scrolledToTop' );
                    }                  
                }    

                // bottom
                if( settings.scrollTop > settings.documentHeight - settings.height - settings.scrollBottomOffset ) {
                    if( !settings.isScrolledToBottom ) {
                        settings.isScrolledToBottom = true;
                        $( document ).trigger( 'win/scrolledToBottom' );
                    }                  
                }   

                if( settings.scrollTop < settings.documentHeight - settings.height - settings.scrollBottomOffset ) {
                    if( settings.isScrolledToBottom ) {
                        settings.isScrolledToBottom = false;
                        $( document ).trigger( 'win/scrolledFromBottom' );
                    }                  
                }  

                // first screen 
                if( settings.scrollTop < settings.height ) {
                    if( !settings.isScrolledToFirstScreen ) {
                        settings.isScrolledToFirstScreen = true;
                        $( document ).trigger( 'win/scrolledToFirstScreen' );
                    }                  
                }  

                if( settings.scrollTop > settings.height ) {
                    if( settings.isScrolledToFirstScreen ) {
                        settings.isScrolledToFirstScreen = false;
                        $( document ).trigger( 'win/scrolledFromFirstScreen' );
                    }                  
                }      

                // last screen 
                if( settings.scrollTop > settings.documentHeight - ( 2* settings.height ) ) {
                    if( !settings.isScrolledToLastScreen ) {
                        settings.isScrolledToLastScreen = true;
                        $( document ).trigger( 'win/scrolledToLastScreen' );
                    }                  
                }  

                if( settings.scrollTop < settings.documentHeight - ( 2* settings.height ) ) {
                    if( settings.isScrolledToLastScreen ) {
                        settings.isScrolledToLastScreen = false;
                        $( document ).trigger( 'win/scrolledFromLastScreen' );
                    }                  
                }    

                // scroll direction
                var delta = settings.scrollTop - settings._scrollTop;

                if( Math.abs( delta ) > 5 ) {
                    if( !settings.ignoreScrollDirection ) {
                        settings._scrollDirection = settings.scrollDirection;
                        if( delta > 0 ) {
                            settings.scrollDirection = 1;
                        } else {
                            settings.scrollDirection = -1;
                        }

                        if( settings._scrollDirection != settings.scrollDirection ) {
                            $( document ).trigger( 'win/changeScrollDirection' );
                        }
                    } else {
                        setTimeout( function() {
                            settings.ignoreScrollDirection = false;
                        }, 100 );
                    }
                }                              
                                                         
            }

            var onScrollDirectionChange = function() {
                debuglog( 'larger.win.onScrollDirectionChange()' );

                var delay = ( settings.scrollDirection > 0 ) ? settings.scrollDownDelay : settings.scrollUpDelay;

                setTimeout( function() {
                    $( 'html' )
                        .removeClass( 'scroll-direction--down' )
                        .removeClass( 'scroll-direction--up' );

                    if( settings.scrollDirection > 0 ) {
                        $( 'html' )
                            .addClass( 'scroll-direction--down' );
                    } else {
                        $( 'html' )
                            .addClass( 'scroll-direction--up' );
                    }
                }, delay );
            }            

            var scrollTo = function( target, offset, animate ) {
                debuglog( 'global.win.scrollTo() target:' );
                debuglog( target );
         
                var top = 0;
         
                // scroll to position
                if( typeof target == 'number' ) {
                    top = target;
                }
         
                // scroll to id
                if( typeof target == 'string' && $( '#' + target ).length > 0 ) {
                    top = parseInt( $( '#' + target ).offset().top );
                }
         
                // scroll to element
                if( typeof target == 'object' && target.length > 0 ) {
                    top = parseInt( target.offset().top );
                }
         
                if( offset ) {
                    top = top + offset;
                } else {
                    top = top + settings.scrollToOffset;
                }

                var distance = Math.floor( Math.abs( top - $( window ).scrollTop() ) );
                var duration = Math.floor( distance / settings.scrollSpeed );
         
                if( animate ) {
                    $( 'html, body' ).animate( {
                        scrollTop: top
                    }, duration );
                } else {
                    settings.element.scrollTop( top );
                }
         
            }             

            var getWidth = function() {
                return settings.width;
            }

            var getHeight = function() {
                return settings.height;
            }

            var getScrollTop = function() {
                return settings.scrollTop;
            }      

            var getScrollFactor = function() {
                return settings.scrollFactor;
            }                 

            var getMousePosition = function() {
                return settings.mousePosition;
            }                     

            return {
                init:               function() { init(); },
                scrollTo:           function( target, offset, animate ) { scrollTo( target, offset, animate ) },
                getWidth:           function() { return getWidth() },
                getHeight:          function() { return getHeight() },
                getMousePosition:   function() { return getMousePosition() },
                getScrollTop:       function() { return getScrollTop() },
                getScrollFactor:    function() { return getScrollFactor() }
            }

        } )();


        // module widgets
        var widgets = ( function() {

            var settings = {
                selector: {
                    widgets: '.widgets',
                    widget:  '.widget',
                    canvas:  '.widgets-canvas'
                },
                element: {},
                delay: 2000,                               

                // arraging
                distanceMin: 16,
                bailout: 100,
                scrollPosition: {
                    x: -100,
                    y: 0
                },
                scrollWidthOriginal: 0,
                scrollWidthClones: 0,
                isScrolling: false,
                isMouseOver: false,
                scrollSpeed: -2,
                scrollSpeedDefault: -2,
                scrollSpeedMax: 3,
                scrollBreakpoint: 100
            };

            var init = function() {
                debuglog( 'siteLarger.widgets.init()' );

                $( settings.selector.canvas ).imagePlaceholder();
                settings.element.canvas = $( settings.selector.canvas );

                fitImages();

                onResize();

                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'win/scroll', function() {

                    } )
                    .on( 'win/loop', function() {
                        onLoop();
                    } )                                                           
                    .on( 'win/resize/finish', function() {
                        onResize();
                    } )
                    .on( 'win/scrolledToTop', function() {

                    } )
                 
                    .on( 'widgets/populated', function() {

                    } );

                    $( settings.selector.widget ).each( function() {
                        var widget = $( this );
                        widget.imagesLoaded( function() {
                            widget.addClass( 'loaded' );
                        } );
                    } );
            }

            var onLoop = function() {

            }

            var onMouseMove = function() {

            }

            var onResize = function() {

            }

            var onScroll = function() {

            }

            var fitImages = function(  ) {
                $( settings.selector.widget ).each( function() {
                    var widget = $( this );
                    var image = widget.find( '.widget-image-image' );
                    
                    if( image.length > 0 ) {
                        var image = image.first();
                        var wrapper = image.closest( 'a' );

                        var imageHeight = parseInt( image.attr( 'height' ) );
                        var imageWidth = parseInt( image.attr( 'width' ) );
                        var imageRatio = imageWidth / imageHeight;

                        var wrapperHeight = wrapper.outerHeight();
                        var wrapperWidth = wrapper.outerWidth();
                        var wrapperRatio = wrapperWidth / wrapperHeight;

                        debuglog( 'widget ratio: ' + imageRatio + ' / ' + wrapperRatio );

                        if( imageRatio > wrapperRatio ) {
                            wrapper.addClass( 'fit-height' );
                        } else {
                            wrapper.addClass( 'fit-width' );                        
                        }
                    }
                } );
            }

            return {
                init: function() { init(); }
            }

        } )();


        // module head
        var head = ( function() {

            var settings = {
                selector: {
                    head: '.head' 
                },
                height: 0,
                isSticky: false
            };

            var init = function() {
                debuglog( 'siteLarger.head.init()' );

                onResizeFinish();
                onScroll();

                debuglog( settings );

                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'win/scroll', function() {
                        onScroll();
                    } )
                    .on( 'win/resize/finish', function() {
                        onResizeFinish();
                    } );
            }

            var onResizeFinish = function() {
                if( !settings.isSticky ) {
                    settings.top = $( settings.selector.head ).offset().top;
                    // settings.top = 0;
                }

                settings.height = $( settings.selector.head ).outerHeight();

                onScroll();
            }

            var onScroll = function() {
                var scrollTop = win.getScrollTop();

                if( scrollTop > settings.top /*+ settings.height */ && !settings.isSticky ) {
                    settings.isSticky = true;
                    $( 'html' ).addClass( 'nav-sticky' );

                    $( document ).trigger( 'head/stick' );
                } else if( scrollTop <= settings.top /*+ settings.height */  && settings.isSticky ) {
                    settings.isSticky = false;
                    $( 'html' ).removeClass( 'nav-sticky' );

                    $( document ).trigger( 'head/unstick' );
                } else {
                    $( 'html' ).removeClass( 'show-head-primary' );                    
                }          
            }

            return {
                init: function() { init(); }
            }

        } )();


        // module 
        var angles = ( function() {

            var settings = {
                selector: {
                    parent: '[data-angles]',
                    angle:  '.angle',
                    svg:    '.angle-svg'
                },
                element: {
                    svg: []
                }
            };

            var init = function() {
                debuglog( 'siteLarger.angles.init()' );

                build();

                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'win/scroll', function() {
                        onScroll();
                    } );
            }

            var onScroll = function() {

            }

            var build = function() {
                debuglog( 'siteLarger.angles.build()' );

                // $( settings.selector.parent ).each( function() {
                //     var parent = $( this );

                //     var angle = $( '<div class="angle"></div>' );

                //     if( parent.attr( 'data-angles' ) == 'top' || parent.attr( 'data-angles' ) == 'both' ) {
                //         angle
                //             .clone()
                //             .addClass( 'angle--top' )
                //             .append( '<div class="angle-inner"></div>' )
                //             .prependTo( parent );
                //     }

                //     var deg = Math.sin( angle.height() / win.getWidth() );
                // } );
            }

            return {
                init: function() { init(); }
            }

        } )();


        // module posts
        var posts = ( function() {

            var settings = {
                selector: {
                    gridContainer:  '[data-grid-role="container"]',
                    gridItems:      '[data-grid-role="item"]',
                    pagination:     '.pagination',
                    paginationNext: '.pagination .next'
                },
                isLoading: false,
            };

            var init = function() {
                debuglog( 'siteLarger.posts.init()' );
                
                settings.nextPageUrl = $( settings.selector.paginationNext ).attr( 'href' );

                buildSizer();
                layout();

                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'click', settings.selector.paginationNext, function( e ) {
                        e.preventDefault();
                        if( !settings.isLoading && settings.nextPageUrl ) {
                            loadNext();
                        }
                    } );
            }   

            var buildSizer = function() {
                debuglog( 'siteLarger.posts.buildSizer()' );

                var sizerItem = $( '<div class="sizer sizer-item"></div>' );

                debuglog( $( settings.selector.gridItems ).first() );
                debuglog( sizerItem );

                sizerItem
                    .insertAfter( $( '.pagination' ) );

                var sizerGutter = $( '<div class="sizer sizer-gutter"></div>' );

                sizerGutter
                    .insertAfter( $( '.pagination' ) );

            }

            var layout = function() {
                debuglog( 'siteLarger.posts.layout()' );

                if( $( settings.selector.gridContainer ).length > 0 ) {

                    $( settings.selector.gridContainer ).imagePlaceholder();

                    settings.layout = $( settings.selector.gridContainer ).masonry( {
                        itemSelector: settings.selector.gridItems,
                        columnWidth: '.sizer-item',
                        gutter: '.sizer-gutter',
                        percentPosition: true,
                        transitionDuration: 0
                    } );

                    setTimeout( function() {
                        $( document ).trigger( 'win/resize/finish' );

                        $( settings.selector.gridContainer )
                            .closest( '.posts' )
                            .addClass( 'layouted' );
                    }, globals.transitionDuration );

                }
            }

            var loadNext = function() {
                    debuglog( 'site.posts.loadNext()' );

                    settings.isLoading = true;
                    
                    if( settings.request ) {
                        settings.request.abort();
                    }
                    
                    $( 'html' )
                        .addClass( 'loading-posts' );

                    $( document ).trigger( 'posts/loading' );

                    settings.request = $.ajax( settings.nextPageUrl, {} )
                        .success( function( data ) { 
                            var posts = $( data ).find( '.post' );
                            var paginationNext = $( data ).find( settings.selector.paginationNext );
                            settings.nextPageUrl = ( paginationNext.length > 0 ) ? paginationNext.attr( 'href' ) : false;

                            addItems( posts );
                            checkPagination();
                        } )
                        .error( function( xhr, status ) {
                            if( status != 'abort' ) {
                                debuglog( 'error: ' + status );
                            }
                        } );

            } 

            var addItems = function( items ) {
                debuglog( 'site.posts.addItems()' );
                
                var count = items.length;
                var added = 0;

                debuglog( count + ' items to add' );

                items.each( function() { 

                    var item = $( this );

                    item
                        .addClass( 'loading' )
                        .redraw()    
                        .insertBefore( $( '.pagination' ) )
                        .imagePlaceholder()
                        .redraw();


                    settings.layout
                        .masonry( 'appended', item );
                } );

                if( window.picturefill ) {
                    picturefill();
                }

                setTimeout( function() {
                    settings.layout
                        .masonry();

                    items
                        .removeClass( 'loading' )
                        .redraw();              

                    settings.isLoading = false;        

                    $( 'html' )
                        .removeClass( 'loading-posts' );

                    setTimeout( function() {
                        $( document ).trigger( 'win/resize/finish' );
                    }, globals.transitionDuration ); 
                                                 
                }, globals.transitionDuration );            

                $( document ).trigger( 'posts/added' );
            }            

            var checkPagination = function() {
                if( !settings.nextPageUrl ) {
                    $( settings.selector.pagination )
                        .addClass( 'disabled' );
                } else {
                    $( settings.selector.pagination )
                        .removeClass( 'disabled' );
                }
            }

            return {
                init: function() { init(); }
            }

        } )();


        // module status
        var status = ( function() {

            var settings = {
                selector: {
                    wrapper:    '.status',
                    time:       '.status--time',
                    weather:    '.status--weather'
                },
                element: {},
                timezone: 'Europe/Amsterdam',
                location: 'Amsterdam, NL'
            };

            var init = function() {
                debuglog( 'siteLarger.status.init()' );
                bindEventHandlers();

                // add moment.js timezone data
                moment.tz.add( ['Europe/Amsterdam|AMT NST NEST NET CEST CET|-j.w -1j.w -1k -k -20 -10|010101010101010101010101010101010101010101012323234545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545454545|-2aFcj.w 11b0 1iP0 11A0 1io0 1cM0 1fA0 1a00 1fA0 1a00 1fA0 1a00 1co0 1io0 1yo0 Pc0 1a00 1fA0 1Bc0 Mo0 1tc0 Uo0 1tA0 U00 1uo0 W00 1s00 VA0 1so0 Vc0 1sM0 UM0 1wo0 Rc0 1u00 Wo0 1rA0 W00 1s00 VA0 1sM0 UM0 1w00 fV0 BCX.w 1tA0 U00 1u00 Wo0 1sm0 601k WM0 1fA0 1cM0 1cM0 1cM0 16M0 1gMM0 1a00 1fA0 1cM0 1cM0 1cM0 1fA0 1a00 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1cM0 1fA0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00 11A0 1qM0 WM0 1qM0 WM0 1qM0 WM0 1qM0 11A0 1o00 11A0 1o00' ] );

                if( $( 'body' ).hasClass( 'home' ) ) {
                    getWeather();
                    startClock();
                }
            }

            var bindEventHandlers = function() {

            }

            var startClock = function() {
                settings.element.time = $( settings.selector.time );

                settings.clock = setInterval( function() {
                    var timeString = moment().tz( settings.timezone ).format( 'H:mm:ss' );

                    // wrap every letter in <span>
                    var letters = timeString.split( '' );
                    timeString = '';
                    $.each( letters, function( i, letter ) {
                        if( letter !== ':' ) {
                            timeString += '<span class="digit">' + letter + '</span>';
                        } else {
                            timeString += letter;
                        }
                    } );

                    settings.element.time.html( timeString );
                }, 250 );
            }

            var getWeather = function() {
                $.simpleWeather( {
                    location: settings.location,
                    woeid: '',
                    unit: 'c',
                    success: function( weather ) {
                        debuglog( weather )

                        $( settings.selector.weather ).text( weather.temp + '°C' );
                    }
                } );
            }

            return {
                init: function() { init(); }
            }

        } )();


        // module carousel
        var carousel = ( function() {

            var settings = {
                selector: {
                    wrapper:        '[data-carousel="wrapper"]',
                    item:           '[data-carousel="item"]',
                    pagination:     '[data-carousel="pagination"]',
                    paginationLink: '[data-carousel="pagination"] > a'
                },
                markup: {
                    pagination:     '<nav class="nav--carousel" role="navigation" data-carousel="pagination"></nav>',
                    paginationLink: '<a href="#"></a>'
                },
                count: 0,
                current: 0,
                interval: 12000
            };

            var init = function() {
                debuglog( 'siteLarger.carousel.init()' );

                bindEventHandlers();

                settings.count = $( settings.selector.item ).length;

                if( settings.count > 0 ) {
                    build();
                    size();
                    set( settings.current );

                    $( settings.selector.wrapper ).addClass( 'initiated' );
                }
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'click', settings.selector.paginationLink, function( e ) {
                        e.preventDefault();

                        var i = parseInt( $( this ).attr( 'data-i' ) );
                        set( i );
                    } ); 
            }

            var build = function() {
                var pagination = $( settings.markup.pagination );

                pagination
                    .insertAfter( $( settings.selector.item + ':last()' ) );

                for( var i = 0; i < settings.count; i++ ) {
                    var link = $( settings.markup.paginationLink );
                    link
                        .text( i )
                        .attr( 'data-i', i )
                        .appendTo( pagination );
                }
            }

            var size = function() {
                var height = 0;

                $( settings.selector.item ).each( function() {
                    var _height = $( this ).outerHeight();
                    if( _height > height ) {
                        height = _height;
                    }

                    $( settings.selector.wrapper ).css( {
                        height: height + 'px'
                    } );                     

                    setTimeout( function() {
                        $( document ).trigger( 'win/resize/finish' );
                    }, globals.transitionDuration );
                } );
            }

            var set = function( i ) {
                clearTimeout( settings.timer );

                if( i != undefined ) {
                    settings.current = i;
                } else {
                    settings.current = ( settings.current < ( settings.count - 1 ) ) ? settings.current + 1 : 0;
                }

                // items
                $( settings.selector.item )
                    .removeClass( 'current' )
                    .eq( settings.current )
                    .addClass( 'current' );

                // pagination
                $( settings.selector.paginationLink )
                    .removeClass( 'current' )
                    .filter( '[data-i="' + settings.current + '"]' )
                    .addClass( 'current' );                    

                settings.timer = setTimeout( function() {
                    set();
                }, settings.interval );
            }

            return {
                init: function() { init(); }
            }

        } )();


        // postImages 
        var postImages = ( function() {

            var settings = {
                selector: {
                    image: '.post-image[data-center-vertically] > img'
                }
            };

            var init = function() {
                debuglog( 'siteLarger.postImages.init()' );

                bindEventHandlers();
                center();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'win/resize/finish', function() {
                        setTimeout( function() {
                            onResize();
                        }, globals.transitionDuration );
                    } );
            }

            var onResize = function() {
                center();
            }

            var center = function() {
                debuglog( 'siteLarger.postImages.center()' );

                if( !Modernizr.objectfit ) {
                    $( settings.selector.image ).each( function() {
                        var image = $( this );
                        var wrapper = image.closest( 'a' );

                        image.imagesLoaded( function() {
                            var heightImage = image.height();
                            var heightWrapper = wrapper.height();

                            var top = ( heightImage - heightWrapper ) / 2 * -1;

                            if( top < 0 ) {
                                image.css( {
                                    'marginTop': top + 'px'
                                } );
                            }

                            wrapper
                                .addClass( 'centered' );
                        } );   
                    } );
                }
            }

            return {
                init: function() { init(); }
            }

        } )();


        // module 
        var module = ( function() {

            var settings = {};

            var init = function() {
                debuglog( 'siteLarger.module.init()' );
                bindEventHandlers();
            }

            var bindEventHandlers = function() {

            }

            return {
                init: function() { init(); }
            }

        } )();

        return {
            init: function() { init(); },
            widgets: widgets
        }

    } )();

    $( document ).ready( function () {
        debuglog( 'site-larger.js loaded...' );

        siteLarger.init();
    } );

} );