jQuery( function( $ ) {

    // app
    siteSmaller = ( function() {

        var init = function() {
            debuglog( 'siteSmaller.init()' );
            
            bindEventHandlers();

            home.init();
            newsletterPopup.init();
        }

        var bindEventHandlers = function() {

        }

        // module home
        var home = ( function() {

            var settings = {
                selector: {
                    main: '.home .main'
                },
                height: 0
            };

            var init = function() {
                debuglog( 'siteSmaller.home.init()' );

                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )   
                    .on( 'win/resize/width/finish', function() {
                        onResize();
                    } );
            }

            var onResize = function() {
                var height = $( window ).innerHeight();
            }

            return {
                init: function() { init(); }
            }
        } ) ();


        // module  newsletterPopup
        var newsletterPopup = ( function() {

            var settings = {
                id: 'newsletterpopup',
                selector: {
                    widget: '.widget_mc4wp_form_widget',
                    input:  '',
                    submit: '',
                    close:  '.newsletterpopup-close'
                }
            };

            var init = function() {
                debuglog( 'siteSmaller.newsletterPopup.init()' );
                bindEventHandlers();

                if( !Cookies.get( 'newsletterpopup' ) || globals.debug || location.href.indexOf( 'newsletterpopup=1' ) > -1 ) {
                    buildPopup();
                    Cookies.set( 'newsletterpopup', '1', { expires: 90 } );                    
                }
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'click', settings.selector.close, function( e ) {
                        e.preventDefault();
                        remove();
                    } );
            }

            var buildPopup = function() {
                debuglog( 'siteSmaller.newsletterPopup.buildPopup()' );

                if( $( settings.selector.widget ).length > 0 ) {
                    var popup = $( settings.selector.widget ).clone();

                    var close = $( '<a class="newsletterpopup-close"></a>' );

                    close
                        .attr( 'href', '#' )
                        .text( 'x' )
                        .appendTo( popup );

                    popup
                        .attr( 'id', settings.id )
                        .addClass( settings.id )
                        .addClass( 'hidden' )
                        .appendTo( $( 'body' ) )
                        .redraw()
                        .removeClass( 'hidden' );
                }
            }

            var remove = function() {
                debuglog( 'siteSmaller.newsletterPopup.remove()' );

                var popup = $( '#' + settings.id );

                $( document )
                    .one( 'transitionend', '#' + settings.id, function() {
                        popup.remove();
                    } );

                popup
                    .addClass( 'hidden' );
            }

            return {
                init: function() { init(); }
            }
        } )();


        // module
        var module = ( function() {

            var settings = {};

            var init = function() {
                debuglog( 'siteSmaller.module.init()' );
                bindEventHandlers();
            }

            var bindEventHandlers = function() {

            }

            return {
                init: function() { init(); }
            }
        } )();


        return {
            init: function() { init(); }
        }

    } )();

    $( document ).ready( function () {
        debuglog( 'site-smaller.js loaded...' );
    
        siteSmaller.init();
    } ); 

} );