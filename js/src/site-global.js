jQuery( function( $ ) {

    // app
    siteGlobal = ( function() {

        var init = function() {
            debuglog( 'siteGlobal.init()' );
            bindEventHandlers();
            win.init();
            nav.init();
            colors.init();
            filter.init();
            pageTransitions.init();
            learnMore.init();
            postImages.init();
            gallery.init();
            contact.init();
            browser.init();
        }

        var bindEventHandlers = function() {

        }

        // module window
        var win = ( function() {

            var settings = {
                scrollToOffset: 0,
                scrollSpeed: 2                  
            };

            var init = function() {
                debuglog( 'siteGlobal.win.init()' );
            
                settings.state = Modernizr.mq( globals.mediaQuery );
                settings.element = $( window );

                settings.width = settings.element.width();
                settings.height = settings.element.height();

                enableFastClick();

                bindEventHandlers();
            }

            var bindEventHandlers = function() {

                // throttle resize event
                settings.element.on( 'resize', function() {
                    
                    if( settings.resizeDelay ) {
                        clearTimeout( settings.resizeDelay );
                        settings.resizeDelay = null;
                    } else {
                        $( 'html' ).addClass( 'resizing' );
                        $( document ).trigger( 'win/resize/start' );
                    }
                 
                    settings.resizeDelay = setTimeout( function() {
                        $( 'html' ).removeClass( 'resizing' );

                        if( settings.width != settings.element.width() ) {
                            $( document ).trigger( 'win/resize/width/finish' );                            
                        }

                        $( document ).trigger( 'win/resize/finish' );
                        settings.resizeDelay = null;

                        // width changed 
                    }, 500 );
                
                } ); 

                $( document ).on( 'win/resize/finish', function() {
                    onResizeFinish();
                } )

            }

            var onResizeFinish = function() {
                debuglog( 'siteGlobal.win.onResizeFinish()' );
                
                settings.width = settings.element.width();
                settings.height = settings.element.height();

                settings._state = settings.state;
                settings.state = Modernizr.mq( globals.mediaQuery );
                
                debuglog( settings );

                if( settings.state != settings._state ) {
                    window.location.reload();
                }
            }

            var enableFastClick = function() {
                FastClick.attach( document.body );                
            }

            var scrollTo = function( target, offset, animate ) {
                debuglog( 'global.win.scrollTo() target:' );
                debuglog( target );
         
                var top = 0;
         
                // scroll to position
                if( typeof target == 'number' ) {
                    top = target;
                }
         
                // scroll to id
                if( typeof target == 'string' && $( '#' + target ).length > 0 ) {
                    top = parseInt( $( '#' + target ).offset().top );
                }
         
                // scroll to element
                if( typeof target == 'object' && target.length > 0 ) {
                    top = parseInt( target.offset().top );
                }
         
                if( offset ) {
                    top = top + offset;
                } else {
                    top = top + settings.scrollToOffset;
                }

                var distance = Math.floor( Math.abs( top - $( window ).scrollTop() ) );
                var duration = Math.floor( distance / settings.scrollSpeed );
         
                if( animate ) {
                    $( 'html, body' ).animate( {
                        scrollTop: top
                    }, duration );
                } else {
                    settings.element.scrollTop( top );
                }
         
            }                

            var getWidth = function() {
                return settings.width;
            }

            var getHeight = function() {
                return settings.height;
            }            

            return {
                init:       function() { init(); },
                scrollTo:   function( target, offset, animate ) { scrollTo( target, offset, animate ) },
                getWidth:           function() { return getWidth() },
                getHeight:          function() { return getHeight() }                
            }

        } )();


        // nav 
        var nav = ( function() {

            var settings = {
                id: 'head-primary'
            };

            var init = function() {
                debuglog( 'siteGlobal.nav.init()' );

                settings.element = $( '.nav--' + settings.id );

                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'head/stick', function() {

                    } )
                    .on( 'head/unstick', function() {

                    } );


                settings.element
                    .on( 'click', '.toggle', function( e ) {
                        e.preventDefault();

                        if( $( 'html' ).hasClass( 'show-' + settings.id  ) ) {
                            $( document ).trigger( 'nav/hide' );
                        } else {
                            $( document ).trigger( 'nav/show' );
                        }

                        $( 'html' ).toggleClass( 'show-' + settings.id );
                    } );
            }

            return {
                init: function() { init(); }
            }
            
        } )();


        // module colors
        var colors = ( function() {

            var settings = {
                faviconSizes: [16,32,48,96]
            };

            var init = function() {
                debuglog( 'siteGlobal.colors.init()' );

                // initiate color.js
                var Color = net.brehaut.Color;

                settings.colors = $.parseJSON( $( 'meta[name="colors"]' ).attr( 'content' ) );

                if( Cookies.get( 'color-index' ) && !$( 'body' ).hasClass( 'home' ) ) {
                    settings.index = parseInt( Cookies.get( 'color-index' ) );
                } else {
                    settings.index = Math.floor( settings.colors.length * Math.random() );                    
                    Cookies.set( 'color-index', settings.index, { expires: 1 } );
                }

                settings.color = settings.colors[ settings.index ];

                var color = Color( settings.color );
                var _color = color;           

                settings.colorLight = color.setLightness( 0.9 ).setSaturation( 0.1 ).toCSS();

                debuglog( settings );

                bindEventHandlers();

                buildStylesheet();
                drawFavicon();
            }

            var bindEventHandlers = function() {

            }

            var buildStylesheet = function() {
                debuglog( 'siteGlobal.colors.buildStylesheet()' );

                vein.inject( ['html','body'], { 'background-color' : settings.color } );
                vein.inject( ['.js .loading-indicator::before'], { 'background-color' : settings.color } );
                vein.inject( ['.js .loading-indicator::after'], { 'border-color' : settings.color } );
                
                vein.inject( ['.widget-tweet .tweet-meta'], { 'color' : settings.color } );
                vein.inject( ['.widget_text a:hover','.widget_text a:focus'], { 'color' : settings.color } );
                vein.inject( ['.newsletterpopup input[type="submit"]'], { 'background-color' : settings.color } );

                vein.inject( ['.logo--head:hover::after', '.logo--head:active::after'], { 'background-color' : settings.color } );
                vein.inject( ['.logo--head > .tagline'], { 'color' : settings.color } );
                // vein.inject( ['.logo--head:hover > .tagline'], { 'color' : settings.color } );

                vein.inject( ['.head-primary-list > li a::after'], { 'background-color' : settings.color } );
                vein.inject( [
                        '.head-primary-list > li.current-menu-item a::after',
                        '.head-primary-list > li.current-menu-parent a::after',
                        '.single-post .head-primary-list > li.news a::after',
                        '.single-cases .head-primary-list > li.cases a::after'
                    ], 
                    { 
                            'background-color' : settings.colorLight 
                    } 
                );
                vein.inject( ['.home-content h2 em::after'], { 'background-color' : settings.color } );
                vein.inject( ['.section--home-clients'], { 'background-color' : settings.colorLight } );

                vein.inject( ['.posts'], { 'background-color' : settings.color } );
                vein.inject( ['.post-image'], { 'background-color' : settings.colorLight } );
                vein.inject( ['.post--tweet h2 > a'], { 'color' : settings.color } );
                vein.inject( ['.post--tweet a'], { 'color' : settings.color } );
                vein.inject( ['.posts--related'], { 'background-color' : settings.colorLight } );
                // vein.inject( ['.pagination .next', '.pagination .prev'], { 'background-color' : settings.color } );
                
                var color = net.brehaut.Color( settings.color );
                vein.inject( ['.filter-nav'], { 'background-color' : color.setAlpha( 0.95 ).toCSS() } );
                var color = net.brehaut.Color( settings.colorLight );
                // vein.inject( ['.filter-toggle'], { 'color' : color.darkenByRatio( 0.2 ) } );


                vein.inject( ['.inline-content ul > li::before'], { 'background-color' : settings.color } );
                vein.inject( ['.inline-content h3'], { 'color' : settings.color } );
                vein.inject( ['.inline-content a'], { 'border-bottom-color' : settings.color } );
                vein.inject( ['.inline-content a:hover', '.inline-content a:focus'], { 'background-color' : settings.colorLight } );
                vein.inject( ['.inline-content ol > li::before'], { 'color' : settings.color } );

                vein.inject( ['.flickity-page-dots .dot'], { 'background-color' : settings.colorLight } );
                vein.inject( ['.flickity-page-dots .dot:hover','.flickity-page-dots .dot:focus','.flickity-page-dots .dot.is-selected'], { 'background-color' : settings.color } );
                vein.inject( ['.flickity-prev-next-button:hover'], { 'background-color' : settings.color } );

                vein.inject( ['.single .post-image'], { 'background-color' : settings.colorLight } );
                vein.inject( ['.single .post-terms > a:hover', '.single .post-terms > a:active'], { 'color' : settings.color } );
                vein.inject( ['.single-content--cases h3::after'], { 'background-color' : settings.color } );

                vein.inject( ['.about--service'], { 'background-color' : settings.color } );
                vein.inject( ['.about--testimonials'], { 'background-color' : settings.colorLight } );
                vein.inject( ['.client--about > a:hover', '.client--about > a:focus'], { 'color' : settings.color } );
                
                vein.inject( ['.initiated [data-carousel="pagination"] > a:hover', '.initiated [data-carousel="pagination"] > a:focus'], { 'background-color' : settings.color } );
                vein.inject( ['.initiated [data-carousel="pagination"] > a.current:hover', '.initiated [data-carousel="pagination"] > a.current:focus'], { 'background-color' : '#fff' } );
                

                vein.inject( ['.divider--footer'], { 'background-color' : settings.color } );
                vein.inject( ['.section--contact a', '.section--contact a:hover', '.section--contact a:focus'], { 'color' : settings.color } );
                vein.inject( ['.footer::after'], { 'background-color' : settings.color } );

                $( 'html' )
                    .addClass( 'colors--set' );
 
                $( document ).trigger( 'colors/set' );
            } 

            var drawFavicon = function() {
                debuglog( 'site.favicon.drawFavicon()' );

                // delete all icons
                $( 'link[rel="icon"]' )
                    .remove();

                // iterate over sizes
                for( var i = 0; i < settings.faviconSizes.length; i++ ) {

                    var size = settings.faviconSizes[i];

                    settings.canvas = document.createElement( 'canvas' );
                    settings.canvas.width = size;
                    settings.canvas.height = size;
                    settings.context = settings.canvas.getContext( '2d' );

                    var centerX = size / 2;
                    var centerY = size / 2;
                    var radius = ( size / 4 );

                    settings.context.beginPath();
                    settings.context.arc( centerX, centerY, radius, 0, ( 2 * Math.PI ), false );
                    settings.context.fillStyle = settings.color;
                    settings.context.fill();

                    // settings.context.fillStyle = color;
                    // settings.context.fillRect( 0, 0, settings.context.canvas.width, settings.context.canvas.height );

                    var src = settings.canvas.toDataURL( 'image/png' );

                    var link = $( '<link rel="icon" type="image/png" sizes="' + settings.faviconSizes[i] + 'x' + settings.faviconSizes[i] + '">' );
                    link.appendTo( 'head' );
                    link.attr( 'href', src );
                }
            }         

            return {
                init: function() { init(); }
            }

        } )();


        // module select
        var filter = ( function() {

            var settings = {
                selector: {
                    wrapper:       '.filter',
                    toggle:        '.filter-toggle',
                    nav:           '.filter-nav',
                    links:         '.filter-nav > a',
                    button_close:  '.filter-ui .close'
                },
                isVisible: false
            }

            var init = function() {
                if( location.hash == '#filter' ) {
                    location.hash = '#';
                }

                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'click', settings.selector.toggle, function( e ) {

                        var select = $( this ).closest( settings.selector.wrapper );
                        show( select );
                    } )
                    .on( 'click', settings.selector.links, function( e ) {
                        var select = $( this ).closest( settings.selector.wrapper );
                        

                        hide( select );
                    } )
                    .on( 'click', settings.selector.button_close, function( e ) {
                        e.preventDefault();

                        var select = $( this ).closest( settings.selector.wrapper );
                        debuglog( 'click close button' );
                        debuglog( select );

                        hide( select );
                    } )

                    // keyboard actions
                    .on( 'keydown', function( e ) {
                        if( settings.isVisible ) {
                            // escape
                            if( e.which == 27 ) {
                                hide();
                            }
                        }
                    } );

                // hashchange
                $( window )
                    .on( 'hashchange', function() {
                        if( settings.isVisible && location.hash != '#filter' ) {
                            hide();
                        }
                    } );
            } 

            var show = function( select ) {
                debuglog( 'global.filter.show()' );
                debuglog( select );

                setTimeout( function() {
                    settings.isVisible = true;
                }, 250 );

                // scroll to top 
                select
                    .find( 'nav' )
                    .scrollTop( 0 );

                select
                    .addClass( 'moved' )
                    .addClass( 'visible' );

                $( 'html' )
                    .addClass( 'show-select' );

                focus();
            }

            var hide = function( select ) {
                debuglog( 'global.filter.hide()' );
                debuglog( select );

                if( !select ) {
                    var select = $( settings.selector.wrapper + '.visible' );
                }
                
                settings.isVisible = false;

                select
                    .removeClass( 'visible' );

                setTimeout( function() {
                    select
                        .removeClass( 'moved' );

                    $( 'html' )
                        .removeClass( 'show-select' );                        
                }, globals.transitionDuration );
            }

            return {
                init: function() { init(); }
            }

        } )(); 


        // module pageTransitions
        var pageTransitions = ( function() {

            var settings = {
                duration: 750,
                selector: 'a[href^="' + globals.blogurl + '"]'
            };

            var init = function() {
                debuglog( 'siteGlobal.pageTransitions.init()' );

                if( !Modernizr.mq( globals.mediaQuery ) ) {
                    settings.selector = settings.selector + ':not( .pagination .next )';
                }

                $( 'html' )
                    .addClass( 'page-transitions' );

                buildLoadingIndicator();
                bindEventHandlers();
            }

            var bindEventHandlers = function() {

                // needed to break BF Cache
                $( window )      
                    .on( 'unload', function( e ) {

                    } );

                $( document )
                    .on( 'mousedown', settings.selector, function( e ) {
                        if( e.button === 0 ) {
                            onClick( $( this ), e );
                        }
                    } );
            }

            var onClick = function( element, event ) {
                var href = element.attr( 'href' );
                hrefDiff = href.split( '#' );
                hrefDiff = hrefDiff[0];
                if( hrefDiff[hrefDiff.length - 1] == '/' ) {
                    hrefDiff = hrefDiff.substr( 0, ( hrefDiff.length - 1 ) );
                }

                var locationDiff = window.location.href;
                locationDiff = locationDiff.split( '#' );
                locationDiff = locationDiff[0];
                if( locationDiff[locationDiff.length - 1] === '/' ) {
                    locationDiff = locationDiff.substr( 0, ( locationDiff.length - 1 ) );
                }                        

                debuglog( locationDiff + ' > ' + hrefDiff );

                if( locationDiff !== hrefDiff ) {
                    event.preventDefault();

                    $( 'html' )
                        .addClass( 'page-transitioning' );

                    setTimeout( function() {
                        window.location = href;                            
                    }, settings.duration );
                }
            }

            var buildLoadingIndicator = function() {
                var indicator = $( '<span class="loading-indicator"></span>' );

                indicator
                    .appendTo( $( 'body' ) );
            }

            return {
                init: function() { init(); }
            }     

        } )();          


        // module learnMore
        var learnMore = ( function() {

            var settings = {
                selector: {
                    link: '.learn-more',
                    target: '.content'
                }
            };

            var init = function() {
                debuglog( 'siteLarger.learnMore.init()' );
                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'click', settings.selector.link, function( e ) {
                        e.preventDefault();

                        win.scrollTo( $( settings.selector.target ), 1, true );
                    } );
            }

            return {
                init: function() { init(); }
            }

        } )();


        // module postImages
        var postImages = ( function() {

            var settings = {
                selector: {
                    image: '.post-image'
                }
            };

            var init = function() {
                debuglog( 'siteGlobal.postImages.init()' );

                check();

                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'posts/added', function() {
                        check();
                    } );
            }

            var check = function() {
                $( settings.selector.image ).each( function() {
                    var image = $( this );
                    
                    image.imagesLoaded( function() {
                        image.addClass( 'loaded' );
                    } ); 
                } );                
            }

            return {
                init: function() { init(); }
            }

        } )();

        // module gallery
        // we use flickity
        // http://flickity.metafizzy.co/
        var gallery = ( function() {

            var settings = {
                selector: {
                    gallery: '.gallery',
                    item:    '.gallery-item'
                }
            };

            var init = function() {
                debuglog( 'siteGlobal.gallery.init()' );

                size();

                $( settings.selector.gallery ).flickity( {
                    setGallerySize: false,
                    wrapAround: true
                } );

                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'win/resize/finish', function() {
                        onResizeFinish();
                    } );
            }   

            var onResizeFinish = function() {
                size();
            }

            var size = function() {
                $( settings.selector.gallery )
                    .each( function() {
                        var gallery = $( this );

                        gallery
                            .css( {
                                'marginLeft': '0px',
                                'marginRight': '0px',
                            } )
                            .redraw();  

                        var left = gallery.offset().left;
                        var right = win.getWidth() - gallery.width() - left;

                        debuglog( left );
                        debuglog( right );

                        debuglog( gallery );

                        gallery
                            .css( {
                                'marginLeft':  ( -1 * left ) + 'px',
                                'marginRight': ( -1 * right ) + 'px',
                            } );

                        if( gallery.hasClass( 'flickity-enabled' ) ) {
                            gallery
                                .flickity( 'resize' );
                        }
                    } );
            }

            return {
                init: function() { init(); }
            }

        } )();


        // module contact
        var contact = ( function() {

            var settings = {
                selector: {
                    link: '[href="#contact"]',
                    target: '#contact'
                }
            };

            var init = function() {
                debuglog( 'siteGlobal.contact.init()' );
                bindEventHandlers();
            }

            var bindEventHandlers = function() {
                $( document )
                    .on( 'click', settings.selector.link, function( e ) {
                        e.preventDefault();

                        win.scrollTo( $( settings.selector.target ), -120, true );

                        $( document ).trigger( 'nav/hide' );
                        $( 'html' ).removeClass( 'show-head-primary' );                        
                    } );
            }

            return {
                init: function() { init(); }
            }

        } )();        



        // module browser
        // we are usiung bowser.js 
        // https://github.com/ded/bowser
        var browser = ( function() {

            var settings = {};

            var init = function() {
                debuglog( 'siteGlobal.browser.init()' );

                // webkit
                if( bowser.webkit ) {
                    $( 'html' ).addClass( 'webkit' );
                } else {
                    $( 'html' ).addClass( 'no-webkit' );
                }             

                bindEventHandlers();
            }

            var bindEventHandlers = function() {

            }

            return {
                init: function() { init(); }
            }

        } )();


        // module 
        var module = ( function() {

            var settings = {};

            var init = function() {
                debuglog( 'siteGlobal.module.init()' );
                bindEventHandlers();
            }

            var bindEventHandlers = function() {

            }

            return {
                init: function() { init(); }
            }

        } )();

        return {
            init: function() { init(); }
        }

    } )();

    // document ready
    $( document ).ready( function () {
        siteGlobal.init();
    } );

} );