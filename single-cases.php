<?php get_header(); ?>

<?php 
	if( have_posts() ) {
		while( have_posts() ) { 
			the_post(); 

            // post image 
		    $image_id = get_post_thumbnail_id( get_the_ID() );

            // meta
            $clients =  get_the_terms( get_the_ID(), 'clients' );
            $sectors =  get_the_terms( get_the_ID(), 'sectors' );
            $disciplines =  get_the_terms( get_the_ID(), 'disciplines' );

?>

<div class="shell">

<?php
    if( $image_id ) {
?>
    <a class="post-image post-image--single post-image--<?php echo get_post_type_advanced(); ?>" data-center-vertically="true">
<?php
        the_responsive_image(
            $image_id,
            array(
                'tiny',
                'thumbnail',
                'medium',
                'large',
                'larger',
                'full'
            ),
            array(
                'class' => '',
                'alt'   => sprintf( __( 'Cover photo from “%s”', 'hm-theme' ), get_the_title() )
            ),
            true,
            true
        );
?>
    </a>
<?php   
    }
?>

	<h1>
        <span class="title">
            <?php the_title(); ?>
        </span>
<?php
    if( $clients && !get_post_meta( get_the_ID(), 'cases--hide-client', true ) ) {
?>
        <span class="client">
            <?php echo wptexturize( $clients[0]->name ); ?>
        </span>
<?php
    }
?>
    </h1>

<?php
    if( $sectors ) {
?>
    <div class="post-terms post-terms--cases post-sector post-sector--<?php echo get_post_type_advanced(); ?>">
        <a href="<?php echo esc_url( get_term_link( $sectors[0] ) ); ?>">
            <?php echo wptexturize( $sectors[0]->name ); ?>
        </a>  
    </div>
<?php
    }
?>

<?php
    if( $disciplines ) {
?>
    <div class="post-terms post-terms--cases post-discipline post-discipline--<?php echo get_post_type_advanced(); ?>">
        <a href="<?php echo esc_url( get_term_link( $disciplines[0] ) ); ?>">
            <?php echo wptexturize( $disciplines[0]->name ); ?>
        </a>  
    </div>
<?php
    }
?>    
		
	<div class="single-content single-content--<?php echo esc_attr( get_post_type_advanced() ); ?> inline-content">   
		<?php the_content(); ?>
	</div>
</div>

<?php get_inc( 'posts', 'related-' . get_post_type_advanced(), false ); ?>

<?php 
		}

	}
?>

<?php get_footer(); ?>