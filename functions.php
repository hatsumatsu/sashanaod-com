<?php
/**
 * i11n
 */
load_theme_textdomain( 'hm-theme', get_stylesheet_directory() . '/languages' );


/** 
 * Register theme CSS
 */
function hm_theme_css() {
    wp_register_style( 'hm-normalize', get_template_directory_uri() . '/css/normalize.css', 0, 'screen' );
    wp_register_style( 'font-averta', get_template_directory_uri() . '/fonts/averta/averta.css', array( 'hm-normalize' ), 0, 'screen' );
    wp_register_style( 'font-merriweather', 'https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic', array( 'hm-normalize' ), 0, 'screen' );
    wp_register_style( 'flickity', get_template_directory_uri() . '/css/flickity.css', array(), 0, 'screen' );
    wp_register_style( 'hm-theme', get_template_directory_uri() . '/style.css', array( 'hm-normalize', 'font-averta', 'font-merriweather', 'flickity' ), 0, 'screen' );

    wp_enqueue_style( 'hm-normalize' );
    wp_enqueue_style( 'font-averta' );
    wp_enqueue_style( 'font-merriweather' );
    wp_enqueue_style( 'flickity' );
    wp_enqueue_style( 'hm-theme' );
} 

add_action( 'wp_enqueue_scripts', 'hm_theme_css' );


/** 
 * Register theme JS 
 */
function hm_theme_js() {
    // load the bundled jQuery in the footer 
    if( !is_admin() ) {  
        wp_deregister_script( 'jquery' );  
        wp_register_script( 'jquery', includes_url( 'js/jquery/jquery.js' ), array(), '1.11.1', true );  
        wp_enqueue_script('jquery');  
    }  

    wp_register_script( 'hm-critical', get_template_directory_uri() . '/js/critical.min.js', array( 'jquery' ), '0.4', true );
    wp_enqueue_script( 'hm-critical' );
} 

add_action( 'wp_enqueue_scripts', 'hm_theme_js' );


/**
 * Register admin CSS
 */
function hm_admin_css() {
    wp_register_style( 'hm-admin', get_template_directory_uri() . '/css/admin.css', array(), 0 );
    wp_enqueue_style( 'hm-admin' );
}

add_action( 'admin_print_styles', 'hm_admin_css' );
add_action( 'admin_print_styles-media-upload-popup', 'hm_admin_css' );


/** 
 * Add inline HTML to <head>
 */
function hm_theme_head() { 
    $colors = array();

    $i = 1;
    for( $i = 1; $i <= 6; $i++ ) {
        $colors[] = get_theme_mod( 'accent_color_' . $i, '#94DCD7' );
    }

    $colorsJSON = json_encode( $colors );
?>
    <meta name="colors" content="<?php echo esc_attr( $colorsJSON ); ?>">

<!--
     <style type="text/css">
        html,
        body { 
            background: <?php echo get_theme_mod( 'accent_color', '#94DCD7' ); ?>; 
        }

        .widget-tweet .tweet-meta {
            color: <?php echo get_theme_mod( 'accent_color', '#94DCD7' ); ?>;
        }
     </style>
-->
<?php
}

add_action( 'wp_head', 'hm_theme_head', 999 );


/** 
 * Add inline HTML to <div id="footer">
 */
function hm_theme_footer() { 
?>
<!-- W3TC-include-js-head -->
<?php 
}

add_action( 'wp_footer', 'hm_theme_footer', 1 );


/** 
 * Remove unneccessary or unsafe <meta> tags
 */
remove_action( 'wp_head', 'rsd_link' );             // remove Really Simple Discovery Entry
remove_action( 'wp_head', 'wlwmanifest_link' );     // remove Windows Live Writer Link 
remove_action( 'wp_head', 'wp_generator' );         // remove Version number


/** 
 * Add theme support 
 */
function hm_theme_setup() {
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'menus' );
    add_theme_support( 
        'html5', 
        array( 
            'comment-list', 
            'comment-form', 
            'search-form', 
            'gallery', 
            'caption' 
        ) 
    );
}

add_action( 'after_setup_theme', 'hm_theme_setup' );


/**
 * Enable page excerpts
 */
function hm_page_excerpts() {
    add_post_type_support( 'page', 'excerpt' );
}

add_filter( 'init', 'hm_page_excerpts' );


/** 
 * Register custom post types and taxonomies
 */
function hm_register_data_structure() {
    register_taxonomy( 
    	'sectors', 
    	array( 
            'cases' 
        ), 
    	array( 
            'hierarchical'      => true,
        	'labels'            => array( 
                'name'                          => __( 'Sectors', 'hm-theme' ), 
                'singular_name'                 => __( 'Sector', 'hm-theme' ),
                'menu_name'                     => __( 'Sectors', 'hm-theme' ),
                'all_items'                     => __( 'All Sectors', 'hm-theme' ),
                'edit_item'                     => __( 'Edit Sector', 'hm-theme' ),
                'view_item'                     => __( 'View Sector', 'hm-theme' ),
                'update_item'                   => __( 'Update Sector', 'hm-theme' ),
                'add_new_item'                  => __( 'Add New Sector', 'hm-theme' ),
                'new_item_name'                 => __( 'New Sector Name', 'hm-theme' ),
                'parent_item'                   => __( 'Parent Sector', 'hm-theme' ),
                'parent_item_colon'             => __( 'Parent Sector:', 'hm-theme' ),
                'search_items'                  => __( 'Search Sectors', 'hm-theme' ),
                'popular_items'                 => __( 'Popular Sectors', 'hm-theme' ),
                'separate_items_with_commas'    => __( 'Separate sectors with commas', 'hm-theme' ), 
                'add_or_remove_items'           => __( 'Add or remove sectors', 'hm-theme' ),
                'choose_from_most_used'         => __( 'Choose from the most used sectors', 'hm-theme' ),
                'not_found'                     => __( 'No sectors found.', 'hm-theme' ),
                'items_list_navigation'         => __( 'Sectors list navigation', 'hm-theme' ),
                'items_list'                    => __( 'Sectors list', 'hm-theme' )                
        	 ),
            'show_ui'           => true,
            'show_admin_column' => true,
            'rewrite'           => array( 
                'slug' => 'cases/filter:sectors' 
            )
        ) 
    );

    register_taxonomy( 
        'disciplines', 
        array( 
            'cases' 
        ), 
        array( 
            'hierarchical'      => true,
            'labels'            => array( 
                'name'                          => __( 'Disciplines', 'hm-theme' ), 
                'singular_name'                 => __( 'Discipline', 'hm-theme' ),
                'menu_name'                     => __( 'Disciplines', 'hm-theme' ),
                'all_items'                     => __( 'All Disciplines', 'hm-theme' ),
                'edit_item'                     => __( 'Edit Discipline', 'hm-theme' ),
                'view_item'                     => __( 'View Discipline', 'hm-theme' ),
                'update_item'                   => __( 'Update Discipline', 'hm-theme' ),
                'add_new_item'                  => __( 'Add New Discipline', 'hm-theme' ),
                'new_item_name'                 => __( 'New Discipline Name', 'hm-theme' ),
                'parent_item'                   => __( 'Parent Discipline', 'hm-theme' ),
                'parent_item_colon'             => __( 'Parent Discipline:', 'hm-theme' ),
                'search_items'                  => __( 'Search Disciplines', 'hm-theme' ),
                'popular_items'                 => __( 'Popular Disciplines', 'hm-theme' ),
                'separate_items_with_commas'    => __( 'Separate disciplines with commas', 'hm-theme' ), 
                'add_or_remove_items'           => __( 'Add or remove disciplines', 'hm-theme' ),
                'choose_from_most_used'         => __( 'Choose from the most used disciplines', 'hm-theme' ),
                'not_found'                     => __( 'No disciplines found.', 'hm-theme' ),
                'items_list_navigation'         => __( 'Disciplines list navigation', 'hm-theme' ),
                'items_list'                    => __( 'Disciplines list', 'hm-theme' )                
             ),
            'show_ui'           => true,
            'show_admin_column' => true,
            'rewrite'           => array( 
                'slug' => 'cases/filter:disciplines' 
            )
        ) 
    );

    register_taxonomy( 
        'clients', 
        array( 
            'cases' 
        ), 
        array( 
            'hierarchical'      => true,
            'labels'            => array( 
                'name'                          => __( 'Clients', 'hm-theme' ), 
                'singular_name'                 => __( 'Client', 'hm-theme' ),
                'menu_name'                     => __( 'Clients', 'hm-theme' ),
                'all_items'                     => __( 'All Clients', 'hm-theme' ),
                'edit_item'                     => __( 'Edit Client', 'hm-theme' ),
                'view_item'                     => __( 'View Client', 'hm-theme' ),
                'update_item'                   => __( 'Update Client', 'hm-theme' ),
                'add_new_item'                  => __( 'Add New Client', 'hm-theme' ),
                'new_item_name'                 => __( 'New Client Name', 'hm-theme' ),
                'parent_item'                   => __( 'Parent Client', 'hm-theme' ),
                'parent_item_colon'             => __( 'Parent Client:', 'hm-theme' ),
                'search_items'                  => __( 'Search Clients', 'hm-theme' ),
                'popular_items'                 => __( 'Popular Clients', 'hm-theme' ),
                'separate_items_with_commas'    => __( 'Separate clients with commas', 'hm-theme' ), 
                'add_or_remove_items'           => __( 'Add or remove clients', 'hm-theme' ),
                'choose_from_most_used'         => __( 'Choose from the most used clients', 'hm-theme' ),
                'not_found'                     => __( 'No clients found.', 'hm-theme' ),
                'items_list_navigation'         => __( 'Clients list navigation', 'hm-theme' ),
                'items_list'                    => __( 'Clients list', 'hm-theme' )                
             ),
            'show_ui'           => true,
            'show_admin_column' => true,
            'rewrite'           => array( 
                'slug' => 'cases/filter:clients' 
            )
        ) 
    );

    register_post_type( 
    	'cases', 
    	array( 
            'labels'            => array( 
                'name'                  => __( 'Cases', 'hm-theme' ), 
                'singular_name'         => __( 'Case', 'hm-theme' ),
                'menu_name'             => __( 'Cases', 'hm-theme' ),
                'menu_admin_bar'        => __( 'Case', 'hm-theme' ),
                'all_items'             => __( 'All Cases', 'hm-theme' ),
                'add_new'               => __( 'Add new', 'hm-theme' ),
                'add_new_item'          => __( 'Add new Case', 'hm-theme' ),
                'edit_item'             => __( 'Edit Case', 'hm-theme' ),
                'new_item'              => __( 'Add Case', 'hm-theme' ),
                'view_item'             => __( 'View Case', 'hm-theme' ),
                'search_items'          => __( 'Search Cases', 'hm-theme' ),
                'not_found'             => __( 'No cases found', 'hm-theme' ),
                'not_found_in_trash'    => __( 'No cases found in trash', 'hm-theme' ),
                'parent_item_colon'     => __( 'Parent Case:', 'hm-theme' ),
                'filter_items_list'     => __( 'Filter Cases list', 'hm-theme' ),
                'items_list_navigation' => __( 'Cases list navigation', 'hm-theme' ),
                'items_list'            => __( 'Cases list', 'hm-theme' )                
    	    ),
            'capability_type'   => 'post',
            'supports'          => array( 
                'title', 
                'editor', 
                'thumbnail' 
            ),
            'public'            => true,
            'menu_position'     => 5,
            'menu_icon'         => 'dashicons-format-aside',
            'rewrite'           => array( 
                'slug' => 'cases' 
            ),
            'has_archive'       => 'cases',
            'taxonomies'        => array( 
                'sectors',
                'disciplines',
                'clients' 
            )
        )
    );

    register_post_type( 
        'testimonials', 
        array( 
            'labels'            => array( 
                'name'                  => __( 'Testimonials', 'hm-theme' ), 
                'singular_name'         => __( 'Testimonial', 'hm-theme' ),
                'menu_name'             => __( 'Testimonials', 'hm-theme' ),
                'menu_admin_bar'        => __( 'Testimonial', 'hm-theme' ),
                'all_items'             => __( 'All Testimonials', 'hm-theme' ),
                'add_new'               => __( 'Add new', 'hm-theme' ),
                'add_new_item'          => __( 'Add new Testimonial', 'hm-theme' ),
                'edit_item'             => __( 'Edit Testimonial', 'hm-theme' ),
                'new_item'              => __( 'Add Testimonial', 'hm-theme' ),
                'view_item'             => __( 'View Testimonial', 'hm-theme' ),
                'search_items'          => __( 'Search Testimonials', 'hm-theme' ),
                'not_found'             => __( 'No cases found', 'hm-theme' ),
                'not_found_in_trash'    => __( 'No cases found in trash', 'hm-theme' ),
                'parent_item_colon'     => __( 'Parent Testimonial:', 'hm-theme' ),
                'filter_items_list'     => __( 'Filter Testimonials list', 'hm-theme' ),
                'items_list_navigation' => __( 'Testimonials list navigation', 'hm-theme' ),
                'items_list'            => __( 'Testimonials list', 'hm-theme' )                
            ),
            'capability_type'   => 'post',
            'supports'          => array( 
                'title', 
                'editor'
            ),
            'public'            => true,
            'menu_position'     => 5,
            'menu_icon'         => 'dashicons-format-chat',
            'rewrite'           => array( 
                'slug' => 'testimonials' 
            ),
            'has_archive'       => 'testimonials',
            'taxonomies'        => array()
        )
    );

}

add_action( 'init', 'hm_register_data_structure' );


/** 
 * Hide admin bar on frontend for all users
 */
add_filter( 'show_admin_bar', '__return_false' );


/** 
 * Register navigation menus 
 */
function register_navigation() {
    register_nav_menu( 'head-primary', __( 'Primary Header Navigation', 'hm-theme' ) );
    register_nav_menu( 'footer-primary', __( 'Primary Footer Navigation', 'hm-theme' ) );
}

add_action( 'after_setup_theme', 'register_navigation' );


/**
 * Add a toggle to the primary navigation menu
 *
 * @param   array   $args options of wp_nav_menu()
 * @return  array   $args
 */
function modify_nav_markup( $args = '' ) {
    $html  = '';
    $html .= '<nav class="nav--' . esc_attr( $args['theme_location'] ) . '" id="nav--' . esc_attr( $args['theme_location'] ) . '" role="navigation">';
    $html .= '<a href="#content" class="skip ' . esc_attr( $args['theme_location'] ) . '-skip" title="' . esc_attr( __( 'Skip Navigation', 'hm-theme' ) ) . '">' . __( 'Skip Navigation', 'hm-theme' ) . '</a>';

    if( $args['theme_location'] == 'head-primary' ) {
        $html .= '<a class="toggle ' . esc_attr( $args['theme_location'] ) . '-toggle" title="' . esc_attr( __( 'Toggle Navigation', 'hm-theme' ) ) . '">' . __( 'Navigation', 'hm-theme' ) . '</a>';
    }
    
    $html .= '<ul class="' . esc_attr( $args['theme_location'] ) . '-list">%3$s</ul>';
    $html .= '</nav>';

    $args['items_wrap'] = $html;
    $args['container']  = false;

    return $args;
}

add_filter( 'wp_nav_menu_args', 'modify_nav_markup' );


/** 
 * Register footer widget area 
 */
function register_widgets() {
    register_sidebar( array(
    	'name'=> __( 'Widgets', 'hm-theme' ),
    	'id' => 'widgets', 
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>'
    	) 
    );	
}

add_action( 'widgets_init', 'register_widgets' );


/** 
 * Define custom image size 
 */
add_image_size( 'tiny', 80, 120, false );
add_image_size( 'larger', 1800, 2700, false );


/** 
 * Define custom jpeg quality 
 */
function hm_jpeg_quality() {
    return 90;
}

add_filter( 'jpeg_quality', 'hm_jpeg_quality' );


/** 
 * Remove unneccessary admin menus 
 */
function hm_remove_admin_menus () {
    // remove_menu_page( 'edit.php' );                  /* Posts */
    // remove_menu_page( 'edit.php?post_type=page' );   /* Pages */
    // remove_menu_page( 'upload.php' );                /* Media */
    remove_menu_page( 'edit-comments.php' );            /* Comments */
    // remove_menu_page( 'tools.php' );                 /* Tools */
}

add_action( 'admin_menu', 'hm_remove_admin_menus' );


/** 
 * Remove unneccessary admin dashboard widgets 
 */
function hm_remove_admin_dashboard_widgets() {
    global $wp_meta_boxes;

	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );
//	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'] );
}

add_action( 'wp_dashboard_setup', 'hm_remove_admin_dashboard_widgets' );


/** 
 * Configure tinyMCE features 
 * http://www.tinymce.com/wiki.php/Configuration
 */
function hm_customize_tinyMCE( $init ) {

    // disable rich text pasting
    $init['paste_as_text'] = true;

    // format select entries
    $init['block_formats'] = __( 'Paragraph', 'hm-theme' ) . '=p;' . __( 'Heading 2', 'hm-theme' ) . '=h2;' . __( 'Heading 3', 'hm-theme' ) . '=h3;' . __( 'Blockquote', 'hm-theme' ) . '=blockquote';


    // style formats
    $style_formats = array(  
        array(  
            'title' => __( 'Bigger', 'hm-theme' ),  
            'block' => 'p',  
            'classes' => 'bigger',
            'wrapper' => false
        ),
        array(  
            'title' => __( 'Smaller', 'hm-theme' ),  
            'block' => 'p',  
            'classes' => 'smaller',
            'wrapper' => false
        )
    );  

    $init['style_formats'] = json_encode( $style_formats );

    return $init;
}

add_filter( 'tiny_mce_before_init', 'hm_customize_tinyMCE' );


/** 
 * Configure tinyMCE button row 1 
 * http://www.tinymce.com/wiki.php/TinyMCE3x:Buttons/controls
 */
function hm_tinyMCE_buttons_1( $buttons ) {
    return array(
        'formatselect',
        'styleselect',
        'bold', 
        'italic', 
        'bullist',
        'numlist', 
        'link', 
        'unlink', 
        'undo', 
        'redo', 
        'removeformat'
    );
}

add_filter( 'mce_buttons', 'hm_tinyMCE_buttons_1' );


/** 
 * Configure tinyMCE button row 2 
 * http://www.tinymce.com/wiki.php/TinyMCE3x:Buttons/controls
 */
function hm_tinyMCE_buttons_2( $buttons ) {
    return array(); 
}

add_filter( 'mce_buttons_2', 'hm_tinyMCE_buttons_2' );


/** 
 * Register tinyMCE editor CSS 
 */
function hm_tinymce_custom_css() {
    add_editor_style( 'css/editor.css' );
}

add_action( 'init', 'hm_tinymce_custom_css' );


/** 
 * Custom excerpt length 
 */
function hm_custom_excerpt_length( $length ) {
    return 40;
}

add_filter( 'excerpt_length', 'hm_custom_excerpt_length' );


/** 
 * Customize auto excerpt
 * + custom word count
 * + Add ellispsis to auto-excerpts when text is too long 
 * + keep basic text formats
 */
function customize_auto_excerpt( $text ) {
    global $post;

    $raw_excerpt = $text;

    if( !$post->post_excerpt || $post->post_excerpt == '' ) {
        $text = get_the_content();
        $text = strip_shortcodes( $text );
        $text = apply_filters( 'the_content', $text );
        $text = str_replace( '\]\]\>', ']]&gt;', $text );
        $text = preg_replace( '@<script[^>]*?>.*?</script>@si', '', $text );
        $text = strip_tags( $text, '<strong><b><em><i><code><p>' );
        $words = explode( ' ', $text, 40 + 1 );

        $words = preg_split( "/[\n\r\t ]+/", $text, 40 + 1, PREG_SPLIT_NO_EMPTY );
        
        if( count( $words ) > 40 ) {
            array_pop( $words );
            array_push( $words, '&hellip;' );
            $text = implode( ' ', $words );

            $text = force_balance_tags( $text );
        } 

    } 

    return apply_filters( 'wp_trim_excerpt', $text, $raw_excerpt );
}

add_filter( 'get_the_excerpt', 'customize_auto_excerpt' );


/** 
 * Custom template part include 
 */
function get_inc( $type, $context, $fallback ) {
    if( $context ) {
        if( is_file( TEMPLATEPATH . '/inc/' . $type . '--' . $context . '.php' ) ) {
            get_template_part( '/inc/' . $type . '--' . $context );
        } else {
            if( $fallback ) {
                get_template_part( '/inc/' . $type );
            }
        }
    } else {
        get_template_part( '/inc/' . $type );
    }
}


/** 
 * Template tag: 
 * the_post_time
 * Display post date and time based on global WP settings.  
 */
function the_post_time() {
    echo get_the_time( get_option( 'date_format' ) ) . ' ' . get_the_time( get_option( 'time_format' ) );
}


/** 
 * Rewrite the search page's permalink 
 * http://example.com/search/{query} 
 */
function hm_rewrite_search() {
    if( is_search() && !empty( $_GET['s'] ) && !is_admin() ) {
        wp_redirect( home_url( '/' . __( 'search', 'hm-theme' ) . '/' ) . urlencode( get_query_var( 's' ) ) );
        exit();
    }
}

add_action( 'template_redirect', 'hm_rewrite_search' );


/**
 * Modify WP's search permalink fragment
 * @param  array $rules original rewrite rules
 * @return array        modified rules
 */
function hm_search_rewrite_rules( $rules ) {
    foreach( $rules as $rule => $target ) {
    
        $new_rule = str_replace( 'search', __( 'search', 'hm-theme' ), $rule );
        unset( $rules[$rule] );
        $rules[$new_rule] = $target;
    }
    return $rules;
}

add_filter( 'search_rewrite_rules', 'hm_search_rewrite_rules' );


/**
 * Force minimum search term length
 */
function cancel_search( $query ) {
    if( is_search() && $query->is_main_query() && strlen( get_query_var( 's' ) ) < 3 ) {
        $query->set( 'post__in', array( 0 ) );
        $query->set( 'error--search-term-length', true );
    }
}

add_action( 'pre_get_posts', 'cancel_search' );


/** 
 * Set search result post count to infinity 
 */
function hm_search_post_count( $query ) {
    if( is_search() && !is_admin() ) {
        $query->set( 'posts_per_page', -1 );
    }
}

add_action( 'pre_get_posts', 'hm_search_post_count' );


/**
 * Set default of link field to 'none' in media library modal
 */
update_option( 'image_default_link_type', 'none' );


/**
 * Get post type wherever get_post_type() is unreliable
 * p.e. outside of loop or on taxonomy archives
 * 
 * @return  string  post type
 */
function get_post_type_advanced() {

    $post_type = get_post_type();

    if( is_post_type_archive() ) {
        $post_type = get_query_var( 'post_type' );
    }

    if( is_tax() ) {
        $post_type = get_taxonomy( get_queried_object()->taxonomy )->object_type[0];
    }

    return $post_type;
}


/**
 * Save IDs of images inserted inline to a post or page
 * to later exclude them from imagelist.php
 * 
 * @param  integer $post_id post ID
 */
function hm_save_inline_images( $post_id ) {
    $post = get_post( $post_id );

    $exclude = array();
    preg_match_all( '/data-id="([0-9]*)"/i', $post->post_content, $exclude );

    $exclude = $exclude[1];

    // convert strings to integers
    foreach( $exclude as &$id ) {
        $id = intval( $id );
    }
    unset( $id );

    // save as postmeta
    update_post_meta( $post_id, 'inline-images', json_encode( $exclude ) );
}

add_action( 'save_post', 'hm_save_inline_images' );


/**
 * Template tag:
 * Get <title> text
 * @param  string $separator String separator
 * @return string            title text
 */
function get_site_title( $separator = ' – ' ) {
    global $wp_query;

    $title = array();

    // global site title
    $title['global'] = get_bloginfo( 'name' );

    // single / page
    $title['single'] = ( is_single() || is_page() ) ? get_the_title() : 0;

    // custom post type 
    $title['post_type'] = 0;
    if( !empty( $wp_query->query['post_type'] ) ) {
        $post_type = get_post_type_object( $wp_query->query['post_type'] );
        $title['post_type'] = $post_type->labels->name;
    }

    // paged
    $title['paged'] = ( !empty( $wp_query->query['paged'] ) ) ? __( 'Page' ) . ' ' . $wp_query->query['paged'] : 0;

    // date archive
    $title['date'] = 0;
    if( is_date() ) {
        if( is_year() ) {
            $title['date'] = $wp_query->query['year'];
        }

        if( is_month() ) {
            $title['date'] = $wp_query->query['monthnum'] . $separator . $wp_query->query['year'];
        }

        if( is_day() ) {
            $title['date'] = $wp_query->query['day'] . $separator . $wp_query->query['monthnum'] . $separator . $wp_query->query['year'];
        }
    }

    // tags
    $title['tag'] = ( is_tag() ) ? __( 'Tag' ) . ': ' . single_cat_title( '', false ) : 0;   

    //categories
    $title['tag'] = ( is_category() ) ? __( 'Category' ) . ': ' . single_cat_title( '', false ) : 0;   

    // custom taxonomy terms
    $title['term'] = 0;
    if( !empty( $wp_query->query_vars['term'] ) ) {
        $taxonomy_labels = get_taxonomy_labels( get_taxonomy( $wp_query->query_vars['taxonomy'] ) );
        $term = get_term_by( 'slug', $wp_query->query_vars['term'], $wp_query->query_vars['taxonomy'] );
        if( $taxonomy_labels && $term ) {
            $title['term'] = $taxonomy_labels->name . ': ' . $term->name;
        }
    }

    // author
    $title['author'] = 0;
    if( !empty( $wp_query->query['author_name'] ) ) {
        $author = get_user_by( 'slug', $wp_query->query['author_name'] );
        $title['author'] = $author->first_name . ' ' . $author->last_name;
    }
        
    $title_string = '';
    $title_string .= ( $title['single'] ) ? $title['single'] . $separator : '';
    $title_string .= ( $title['term'] ) ? $title['term'] . $separator : '';
    $title_string .= ( $title['tag'] ) ? $title['tag'] . $separator : '';
    $title_string .= ( $title['post_type'] ) ? $title['post_type'] . $separator : '';
    $title_string .= ( $title['date'] ) ? $title['date'] . $separator : '';
    $title_string .= ( $title['author'] ) ? $title['author'] . $separator : '';
    $title_string .= ( $title['paged'] ) ? $title['paged'] . $separator : '';
    $title_string .= $title['global'];

    return $title_string;
}


/**
 * Modify the separator of the automatic <title>
 * @param  [type] $separator [description]
 * @return [type]            [description]
 */
function modify_document_title_separator( $separator ){
    $separator = '•';    
    
    return $separator; 
}

add_filter( 'document_title_separator', 'modify_document_title_separator', 10 );


/**
 * Modify the parts of the automatic <title>
 * @param  array $title original title parts
 * @return array        modified title parts
 */
function modify_post_title( $title ) {
    if( is_home() ) { 
        $title['tagline'] = null; 
    }

    if( is_front_page() ) { 
        $title['title'] = null; 
    }    
    
    return $title; 
}

add_filter( 'document_title_parts', 'modify_post_title', 10 );


/**
 * Template tag:
 * Get <meta> description
 * @return string description text
 */
function get_site_description() {
    global $wp_query,
           $post;

    $description = get_bloginfo( 'description' );

    // single / page
    if( ( is_single() || is_page() ) && !is_front_page() ) {
        $excerpt = $post->post_content;
        $description = ( !empty( $excerpt ) ) ? $excerpt : $description;
    }

    // custom taxonomy terms
    if( !empty( $wp_query->query_vars['term'] ) ) {
        $term = get_term_by( 'slug', $wp_query->query_vars['term'], $wp_query->query_vars['taxonomy'] );
        if( $term ) {
            $description = ( !empty( $term->description ) ) ? $term->description : $description;
        }
    }

    // tags
    if( is_tag() ) {
        $tag = get_term_by( 'slug', $wp_query->query['tag'], 'post_tag' );
        if( $tag ) {
            $description = ( !empty( $tag->description ) ) ? $tag->description : $description;
        }
    }

    // category
    if( is_category() ) {
        $category = get_term_by( 'slug', $wp_query->query['category_name'], 'category' );
        if( $category ) {
            $description = ( !empty( $category->description ) ) ? $category->description : $description;
        }
    }

    // author
    if( !empty( $wp_query->query['author_name'] ) ) {
        $author = get_user_by( 'slug', $wp_query->query['author_name'] );
        if( $author ) {
            $biography = get_the_author_meta( 'description', $author->ID );
            $description = ( !empty( $biography ) ) ? $biography : $description;
        }    
    }

    return wp_trim_words( $description, 115, null );
}


/**
 * Get <meta> image
 * @return string description text
 */
function get_site_image() {
    global $wp_query,
           $post;

    $url = '';

    // $page = get_page_by_title( 'Start' );
    // if( $page && $image_id = get_post_thumbnail_id( $page->ID ) ) {
    //     $image = wp_get_attachment_image_src( $image_id, 'large' );
    //     $url = $image[0];
    // }

    // single / page
    if( is_single() || is_page() ) {
        if( $image_id = get_post_thumbnail_id( $post->ID ) ) {
            $image = wp_get_attachment_image_src( $image_id, 'large' );
            $url = $image[0];
        }
    }

    return $url;
}


/**
 * Print responsive image template tag
 * Needs picturefill.js
 * Example:
 * the_responsive_image( 
 *     $image_id, 
 *     array( 
 *         'medium', 
 *         'large', 
 *         'full' 
 *     ), 
 *     array( 
 *         'sizes' => '100vw', 
 *         'alt' => 'Alt text', 
 *         'class' => 'wp-image' 
 *     ),
 *     true,
 *     true 
 *  )
 * 
 * @param  integer $id               image ID
 * @param  array $sizes              array of image size key words
 * @param  array $attributes         array of attribute / value pairs
 * @param  boolean $dimensions       add dimension attributes to <img>
 * @param  boolean|string $fallback  true to use first item in sizes array as fallback src, string to define custom fallback image size
 * @return string                    HTML <img> tag
 */
function the_responsive_image( $id, $sizes = array( 'medium', 'large', 'full' ), $attributes = array(), $dimensions = false, $fallback = false ) {
    echo get_the_responsive_image( $id, $sizes, $attributes, $dimensions, $fallback );
}


/**
 * Get responsive image template tag
 * Needs picturefill.js
 * Example:
 * get_the_responsive_image( 
 *     $image_id, 
 *     array( 
 *         'medium', 
 *         'large', 
 *         'full' 
 *     ), 
 *     array( 
 *         'sizes' => '100vw', 
 *         'alt' => 'Alt text', 
 *         'class' => 'wp-image' 
 *     ),
 *     true,
 *     true 
 *  )
 * 
 * @param  integer $id               image ID
 * @param  array $sizes              array of image size key words
 * @param  array $attributes         array of attribute / value pairs
 * @param  boolean $dimensions       add dimension attributes to <img>
 * @param  boolean|string $fallback  true to use first item in sizes array as fallback src, string to define custom fallback image size
 * @return string                    HTML <img> tag
 */
function get_the_responsive_image( $id, $sizes = array( 'medium', 'large', 'full' ), $attributes = array(), $dimensions = false, $fallback = false ) {
    $html = '';
    $html .= '<img';

    // srcset
    $srcset = '';
    foreach( $sizes as $size ) {
        $src = wp_get_attachment_image_src( $id, $size );
        $srcset .= $src[0] . ' ' . $src[1] . 'w,';

        if( $dimensions ) {
            $width = $src[1];
            $height = $src[2];
        }
    }

    $srcset = rtrim( $srcset, ',' );
    $attributes['srcset'] = $srcset;

    // dimensions
    if( $dimensions ) {
        $attributes['width'] = $width;
        $attributes['height'] = $height;
    }

    // src
    if( $fallback ) {
        if( is_string( $fallback ) ) {
            $src = wp_get_attachment_image_src( $id, $fallback );
        } else  {
            $src = wp_get_attachment_image_src( $id, $sizes[0] );        
        }

        $attributes['src'] = $src[0];        
    }

    // attributes
    foreach( $attributes as $attribute => $value ) {
        $html .= ' ' . $attribute . '="' . esc_attr( $value ) . '"';
    }    

    $html .= '>';

    return $html;
}


/**
 * Modify the markup of inline images added via the visual editor
 * to follow the srcset responsive image pattern
 *
 * This filter is used when an image is added via the media modal.
 * Do not add <figure> tags here, because when we add a caption later 
 * we end up with a nested <figure>[caption]<img> structure...
 * @param  string $html  original image markup
 * @param  int    $id    attachment ID
 * @param  string $alt   alt text 
 * @param  string $title image title
 * @return string        modified markup
 */
function responsive_image_embed( $html, $id, $alt, $title, $align = null, $size = null ) {
    $html = '';

    $orientation = get_post_meta( $id, 'orientation', true );

    $class = '';
    $class .= ( $orientation ) ? ' orientation--' . $orientation : '';
    $class .= ( $align ) ? ' ' . $align : '';    

    $html .= get_the_responsive_image( 
        $id, 
        array(
            'thumbnail',
            'medium',
            'large',
            'full'
        ),
        array( 
            'sizes'   => '100vw',
            'alt'     => $alt,
            'title'   => $title,
            'class'   => 'inline-image ' . $class,
            'data-id' => $id
        ), 
        true,
        true 
    );

    return $html;
}

add_filter( 'get_image_tag', 'responsive_image_embed', 10, 6 );


/**
 * Modify output of [caption] shortcode
 * @param  ?? $empty        ??
 * @param  array $attr      shortcode attributes
 * @param  string $content  the image markup inside [caption][/caption]
 * @return strings          HTML makrup          
 */
function modify_caption_shortcode( $empty, $attr, $content ){
    $attr = shortcode_atts( array(
        'id'      => '',
        'align'   => '',
        'width'   => '',
        'caption' => ''
    ), $attr );

    $id = ( $attr['id'] ) ? intval( str_replace( 'attachment_', '', $attr['id'] ) ) : null;

    // hack to get the orientation of the child image
    if( strpos( $content, 'orientation--portrait' ) != false  ) {
        $orientation = 'portrait';
    } elseif( strpos( $content, 'orientation--square' ) != false ) {
        $orientation = 'square';
    } elseif( strpos( $content, 'orientation--landscape' ) != false ) {
        $orientation = 'landscape';
    } else {
        $orientation = false;
    }

    $class = '';
    $class .= ( $orientation ) ? ' orientation--' . $orientation : '';
    $class .= ( $attr['align'] ) ? ' ' .  $attr['align'] : '';


    $html = '';
    $html .= '<figure class="' . esc_attr( $class ) . '" data-id="' . esc_attr( $id ) . '">';

    $html .= do_shortcode( strip_tags( $content, '<img><img/><figcaption>' ) );

    if( $attr['caption'] ) {
        $html .= '<figcaption>';
        $html .= $attr['caption'];
        $html .= '</figcaption>';
    }

    $html .= '</figure>';

    return $html;
}

add_filter( 'img_caption_shortcode', 'modify_caption_shortcode', 10, 3 );


/**
 * Customize inline gallery markup
 * @param  array $attributes  gallery attributes
 * @return string             HTML markup
 */
function hm_gallery_shortcode( $attributes ) {
    $sizes = array(
        'thumbnail',
        'medium',
        'large',
        'larger',
        'full'
    );

    $output = '';
    $ids = explode( ',', $attributes['ids'] );

    if( $attributes['ids'] ) {
        $id = uniqid();
        $images = get_posts(
            array(
                'post_type'         => 'attachment',
                'post_mime_type'    => 'image',
                'include'           => $ids,
                'orderby'           => 'post__in'
            )
        );

        if( $images ) {
            $output .= '<div class="gallery gallery--' . esc_attr( $id ) . '">';

            foreach( $images as $image ) {
                $output .= '<figure class="gallery-item">';                
                $output .= get_the_responsive_image( 
                    $image->ID, 
                    array(
                        'thumbnail',
                        'medium',
                        'large',
                        'larger',
                        'full'
                    ), 
                    array(
                        'sizes' => '100vw',  
                        'class' => 'gallery-image'
                    ), 
                    true,
                    true
                );            

                $output .= '</figure>';
            }

            $output .= '</div>';
        }
    }

    return $output;
}

remove_shortcode( 'gallery' );
add_shortcode( 'gallery', 'hm_gallery_shortcode' );



/**
 * Force minimum images dimensions on upload
 * @param  array $file file object
 * @return array       file object
 */
function minimum_image_dimensions( $file ) {
    $minimum = array( 
        'width' => 400, 
        'height' => 300 
        );
    
    $mimes = array( 
        'image/jpeg', 
        'image/png', 
        'image/gif' 
        );

    if( !in_array( $file['type'], $mimes ) ) { 
        return $file;
    }

    $image = getimagesize( $file['tmp_name'] );

    if ( $image[0] < $minimum['width'] ) {
        $file['error'] = sprintf( __( 'Image is too small. Minimum width is %1$spx. Uploaded image width is %2$spx', 'hm-theme' ), $minimum['width'], $image[0] );
    } elseif ( $image[1] < $minimum['height'] ) {
        $file['error'] = sprintf( __( 'Image is too small. Minimum height is %1$spx. Uploaded image height is %2$spx', 'hm-theme' ), $minimum['height'], $image[0] );
    }

    return $file;
}

add_filter( 'wp_handle_upload_prefilter', 'minimum_image_dimensions' ); 


/**
 * Add custom meta data to uploaded images
 * + orientation
 * @param  array $metadata original meta data
 * @param  int   $id       attachment ID
 * @return array           modified meta data
 */
function add_image_meta_data( $metadata, $id ) {    
    // orientation
    if( $metadata['width'] && $metadata['height'] ) {
        if( intval( $metadata['width'] ) == intval( $metadata['height'] ) ) {
            $orientation = 'square';
        } elseif( intval( $metadata['width'] ) < intval( $metadata['height'] ) ) {
            $orientation = 'portrait';
        } else {
            $orientation = 'landscape';        
        }

        update_post_meta( $id, 'orientation', $orientation );   
    }  

    return $metadata;
}

add_filter( 'wp_generate_attachment_metadata', 'add_image_meta_data', 10, 2 );


/**
 * Get the current archive URL without page number 
 * @return  int  $url URL of the archive 
 */
function get_current_archive_url() {
    global $wp;

    $url = home_url( $wp->request );
    $url = explode( 'page/', $url );
    $url = trailingslashit( $url[0] );

    return $url;
}


/**
 * Get the theme directory name
 * @return string directory name
 */
function get_theme_directory_name() {
    $url = get_template_directory_uri();
    $url = str_replace( '/', '', str_replace( get_theme_root_uri(), '', $url ) );

    return $url;
}


/**
 * Wrap oembed in <div>
 * @param  string $html  original markup
 * @param  string $url  URL
 * @param  array $attributes  config
 * @param  int $id  post ID
 * @return string   modified markup
 */
function wrap_oembed( $html, $url, $attributes, $id ) {
    $class = '';

    if( strpos( $url, 'youtu' ) || strpos( $url, 'vimeo' ) ) {
        $class .= ' embed-video';
    }

    return '<div class="' . esc_attr( $class ) . '">' . $html . '</div>';
}

add_filter( 'embed_oembed_html', 'wrap_oembed', 99, 4 );


/**
 * Register custom theme options
 * @param  [type] $wp_customize [description]
 * @return [type]               [description]
 */
function register_theme_options( $wp_customize ) {
    // register setting
    $wp_customize->add_setting( 
        'accent_color_1', 
        array()
    );

    $wp_customize->add_setting( 
        'accent_color_2', 
        array()
    );

    $wp_customize->add_setting( 
        'accent_color_3', 
        array()
    );

    $wp_customize->add_setting( 
        'accent_color_4', 
        array()
    );

    $wp_customize->add_setting( 
        'accent_color_5', 
        array()
    );

    $wp_customize->add_setting( 
        'accent_color_6', 
        array()
    );                    

    // register control
    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'accent_color_1', 
            array(
                'label'      => __( 'Accent Color 1', 'hm-theme' ),
                'section'    => 'colors',
                'settings'   => 'accent_color_1',
            ) 
        ) 
    );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'accent_color_2', 
            array(
                'label'      => __( 'Accent Color 2', 'hm-theme' ),
                'section'    => 'colors',
                'settings'   => 'accent_color_2',
            ) 
        ) 
    );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'accent_color_3', 
            array(
                'label'      => __( 'Accent Color 3', 'hm-theme' ),
                'section'    => 'colors',
                'settings'   => 'accent_color_3',
            ) 
        ) 
    );

    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'accent_color_4', 
            array(
                'label'      => __( 'Accent Color 4', 'hm-theme' ),
                'section'    => 'colors',
                'settings'   => 'accent_color_4',
            ) 
        ) 
    );


    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'accent_color_5', 
            array(
                'label'      => __( 'Accent Color 5', 'hm-theme' ),
                'section'    => 'colors',
                'settings'   => 'accent_color_5',
            ) 
        ) 
    );
    
    $wp_customize->add_control( 
        new WP_Customize_Color_Control( 
            $wp_customize, 
            'accent_color_6', 
            array(
                'label'      => __( 'Accent Color 6', 'hm-theme' ),
                'section'    => 'colors',
                'settings'   => 'accent_color_6',
            ) 
        ) 
    );   

}

add_action( 'customize_register', 'register_theme_options' );



/**
 * Modify queries
 */
function modify_query( $query ) {
    if( !is_admin() && $query->is_main_query() ) {

        if( is_home() ) {
            $query->set( 'post_type', array( 'post', 'tweet' ) );
            $query->set( 'posts_per_page', 12 );
        }

        if( is_archive() && ( get_query_var( 'post_type' ) === 'cases' 
            || get_query_var( 'taxonomy' ) === 'clients'
            || get_query_var( 'taxonomy' ) === 'sectors'
            || get_query_var( 'taxonomy' ) === 'disciplines'
            ) 
        ) {
            $query->set( 'posts_per_page', 6 );            
            $query->set( 'orderby', 'menu_order' );            
            $query->set( 'order', 'ASC' );            
        }
    }
}

add_filter( 'pre_get_posts', 'modify_query' );


/**
 * Add capablities to editor role
 */
function add_editor_capabilities() {
    $role = get_role( 'editor' );

    // edit theme options
    $role->add_cap( 'edit_theme_options' ); 
}

add_action( 'admin_init', 'add_editor_capabilities' );


/**
 * Redirect pages
 * + single tweet > home
 */
function redirect_views() {
    global $wp_query;

    // print_r( $wp_query );

    if( $wp_query->is_main_query() ) {

        // redirect single tweet to home
        if( is_single() && get_query_var( 'post_type' ) === 'tweet' ) {
            wp_redirect( get_bloginfo( 'url' ) );
            exit();
        }

    }
}

add_action( 'template_redirect', 'redirect_views' );


/**
 * Allow SVG uploads
 * @param  array $mimes allowed mime types
 * @return array        allowed mime types
 */
function allow_svg_uploads( $mimes ) {
  $mimes['svg'] = 'image/svg+xml';
  
  return $mimes;
}

add_filter( 'upload_mimes', 'allow_svg_uploads' );