<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head data-wpurl="<?php bloginfo( 'url' ); ?>" data-project="<?php echo esc_attr( get_theme_directory_name() ); ?>">
    <meta charset="utf-8">
    
    <?php wp_head(); ?>

    <script>
        document.getElementsByTagName( 'html' )[0].className += ' js';
        document.getElementsByTagName( 'html' )[0].className = document.getElementsByTagName( 'html' )[0].className.replace( ' no-js', '' );
    </script>

    <?php get_inc( 'head', 'facebook', 0 ); ?>
    <?php get_inc( 'head', 'twitter', 0 ); ?>

    <meta name="description" content="<?php echo esc_attr( get_site_description() ); ?>">
    
    <meta name="HandheldFriendly" content="True">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="cleartype" content="on"> 

    <link rel="icon" href="<?php bloginfo( 'template_directory' ); ?>/img/favicon.png" type="image/png">
</head>
<body <?php body_class(); ?>>

    <section class="head" role="banner">
        <a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" class="logo logo--head">
            <span class="name">
                <?php bloginfo( 'name' ); ?>
            </span>
            <span class="tagline">
                <?php echo get_bloginfo( 'description' ); ?>
            </span>
        </a>
            
            <?php get_inc( 'nav', 'head-primary', 0 ); ?>
            
    </section><!-- end .head -->

    <?php get_inc( 'widgets', 'footer', 0 ); ?>

    <section class="main" data-angles="both">

    	<section class="content" role="main" id="content">