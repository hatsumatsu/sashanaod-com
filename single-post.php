<?php get_header(); ?>

<?php 
    global $terms;

	if( have_posts() ) {
		while( have_posts() ) { 
			the_post(); 

		    $image_id = get_post_thumbnail_id( get_the_ID() );

		    // terms
		    $_terms =  get_the_terms( get_the_ID(), 'category' );
		    $terms = array();
		    foreach( $_terms as $term ) {
		        if( $term->name != 'Uncategorized' ) {
		            $terms[] = $term;
		        }
		    }
?>
<div class="shell">

<?php
    if( $image_id ) {
?>
    <a class="post-image post-image--single post-image--<?php echo get_post_type_advanced(); ?>" data-center-vertically="true">
<?php
        the_responsive_image(
            $image_id,
            array(
                'tiny',
                'thumbnail',
                'medium',
                'large',
                'larger',
                'full'
            ),
            array(
                'class' => '',
                'alt'   => sprintf( __( 'Cover photo from “%s”', 'hm-theme' ), get_the_title() )
            ),
            true,
            true
        );
?>
    </a>
<?php   
    }
?>

	<h1><?php the_title(); ?></h1>

<?php
    if( $terms ) {
?>
    <div class="post-terms post-terms--<?php echo get_post_type_advanced(); ?>">
<?php
        foreach( $terms as $term ) {
?>
        <a href="<?php echo esc_url( get_term_link( $term ) ); ?>">
            <?php echo wptexturize( $term->name ); ?>
        </a>
<?php
        }
?>        
    </div>
<?php
    }
?>
    <time class="post-time post-time--<?php echo get_post_type_advanced(); ?>" datetime="<?php the_time( 'Y-m-d\TH:i' ); ?>">
        <?php the_time( 'j' ); ?>&thinsp;/&thinsp;<?php the_time( 'n' ); ?>&thinsp;/&thinsp;<?php the_time( 'Y' ); ?>
    </time>
		
	<div class="single-content single-content--<?php echo esc_attr( get_post_type_advanced() ); ?> inline-content">   
		<?php the_content(); ?>
	</div>
</div>

<?php get_inc( 'posts', 'related-' . get_post_type_advanced(), false ); ?>

<?php 
		}

	}
?>

<?php get_footer(); ?>